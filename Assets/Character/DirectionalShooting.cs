﻿using UnityEngine;
using UnityEngine.Animations.Rigging;

public class DirectionalShooting : MonoBehaviour
{

    [SerializeField]
    private Transform target;

    // References
    private Camera mainCamera;

    private void Awake()
    {
        mainCamera = Camera.main;
    }

    void Update()
    {
        Vector3 mousePosition = Input.mousePosition;
        Ray screenRay = mainCamera.ScreenPointToRay(mousePosition);

        // Raycast against the ground
        if(Physics.Raycast(screenRay, out RaycastHit hitInfo))
        {
            target.transform.position = hitInfo.point;
        }

        // Left mouse click
        if (Input.GetMouseButton(0))
        {

        }
    }
}
