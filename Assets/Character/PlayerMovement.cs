﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PlayerMovement : MonoBehaviour
{

    [SerializeField]
    private float walkSpeed = 5.0f;
    [SerializeField]
    private float sprintSpeed = 5.0f;

    private float maxSpeed;

    // References
    private CharacterController cc;
    private Animator animator;

    private Vector3 currentVelocity;
    private float x, y;
    private bool isSprinting = false;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        y = Input.GetAxis("Vertical");
        x = Input.GetAxis("Horizontal");

        isSprinting = Input.GetKey(KeyCode.LeftShift);

        animator.SetFloat("y", y);
        animator.SetFloat("x", x);
    }
}
