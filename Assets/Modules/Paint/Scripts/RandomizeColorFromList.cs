﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomizeColorFromList : MonoBehaviour
{
    [SerializeField]
    private string colorAttribute = "_MainColor";

    [SerializeField]
    private ColorListData colorList;

    private Material material;

    private void Awake()
    {
        material = GetComponent<Renderer>().material;

        material.SetColor(colorAttribute, GetNewColor());
    }

    protected Color GetNewColor()
    {
        if (colorList == null) return Color.magenta;
        return colorList.colors[Random.Range(0, colorList.colors.Count)];
    }
}
