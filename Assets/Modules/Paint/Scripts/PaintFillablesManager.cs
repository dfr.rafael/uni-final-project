﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class PaintVertexIsFilled {
    public PaintVertexColorDistance paintFillable;
    public bool isFilled;

    public PaintVertexIsFilled(PaintVertexColorDistance paintFillable)
    {
        this.paintFillable = paintFillable;
        isFilled = false;
    }
}

public class PaintFillablesManager : MonoBehaviour
{

    [SerializeField]
    private bool getPaintables = true;

    [SerializeField]
    private List<PaintVertexIsFilled> paintFillables = new List<PaintVertexIsFilled>();

    [SerializeField]
    private UnityEvent onAllFilled;

    private bool onAllFilledInvoked = false;

    private void Awake()
    {
        onAllFilled.AddListener(() =>
        {
            Debug.Log("All paintables have been filled.");
        });

        if (getPaintables)
        {
            var childrenPaintFillables = GetComponentsInChildren<PaintVertexColorDistance>();
            foreach (var paint in childrenPaintFillables)
            {
                paintFillables.Add(new PaintVertexIsFilled(paint));
            }
        }

        Debug.Log(paintFillables.Count);

        paintFillables.ForEach((paint) => {
            Health health = paint.paintFillable.GetComponent<Health>();
            Debug.Log("Adding event");
            health?.AddOnMaxValue(() => {
                paint.isFilled = true;
                CheckAllPaintFillables();
            });
        });
    }

    private void CheckAllPaintFillables()
    {
        if(onAllFilledInvoked) { return; }

        for (int idx = paintFillables.Count - 1; idx >= 0; idx--)
        {
            if (!paintFillables[idx].isFilled) {
                return; 
            }
        }

        onAllFilledInvoked = true;
        onAllFilled.Invoke();
    }

}
