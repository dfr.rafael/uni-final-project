﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomizeColor : MonoBehaviour
{

    [SerializeField]
    private string colorAttribute = "_MainColor";

    [Header("Ranges")]
    [SerializeField, Tooltip("From 0 to 360.")]
    private Vector2 minMaxHue = new Vector2(0f, 360f);
    [SerializeField, Tooltip("From 0 to 100.")]
    private Vector2 minMaxSaturation = new Vector2(0f, 100f);
    [SerializeField, Tooltip("From 0 to 100.")]
    private Vector2 minMaxValue = new Vector2(0f, 100f);

    private Material material;

    private void Start()
    {
        material = GetComponent<Renderer>().material;

        material.SetColor(colorAttribute, GetNewColor());
    }

    protected Color GetNewColor()
    {
        float h = minMaxHue.Random() / 360f;
        float s = minMaxSaturation.Random() / 100f;
        float v = minMaxValue.Random() / 100f;

        return Color.HSVToRGB(h, s, v);
    }
}
