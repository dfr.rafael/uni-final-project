﻿using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintFillable : MonoBehaviour {

    //
    // VALUES
    //

    [SerializeField]
    private Vector2 minMaxHealthRemap = new Vector2(0f, 1f);

    [SerializeField]
    protected Health health;

    [SerializeField]
    private bool useTimedLerp = true;

    [SerializeField, Range(0.01f, 1.0f)]
    private float timedLerpDuration = 0.1f;

    //
    // CACHE
    //
    private Material[] materials;
    private Coroutine currentCoroutine = null;

    private void Awake()
    {
        if (health)
            health = GetComponent<Health>();

        // Setting paint material
        var renderer = GetComponentsInChildren<Renderer>();

        materials = new Material[renderer.Length];
        for (int index = 0; index < renderer.Length; index++)
        {
            materials[index] = renderer[index].material;
        }
    }

	private void Start()
	{
        Reset();
    }

	private void Reset()
    {
        SetPaintValue(0f);
    }

    public void SetPaintValue(float t)
    {
        if (useTimedLerp) {
            if (currentCoroutine != null) StopCoroutine(currentCoroutine);
            currentCoroutine = StartCoroutine(LerpPaintValue(t));
        }
        else
        {
            float paintValue = PaintValueRemap(t);

            for (int index = 0; index < materials.Length; index++)
            {
                materials[index].SetFloat("_PaintValue", paintValue);
            }
        }
    }

    // Avoid messing up with the uv or some shit    
    // Remaping cause the shader needs specifics UVs and I ain't got time for that
    private float PaintValueRemap(float t)
    {
        float paintValue = t.Remap(0f, 1f, minMaxHealthRemap.x, minMaxHealthRemap.y);
        if (t == 0f) paintValue -= 1f;
        else if (t == 1f) paintValue += 1f;
        return paintValue;
    }

    private IEnumerator LerpPaintValue(float newPaintValue)
    {
        newPaintValue = PaintValueRemap(newPaintValue);

        List<float> paintValues = new List<float>();
        for (int index = 0; index < materials.Length; index++)
        {
            paintValues.Add(Mathf.Clamp(materials[index].GetFloat("_PaintValue"), minMaxHealthRemap.x, minMaxHealthRemap.y));
        }

        float t = 0.0f;
        while (t < timedLerpDuration)
        {
            t += Time.deltaTime;
            float normalizedT = t / timedLerpDuration;

            for (int index = 0; index < materials.Length; index++)
            {
                materials[index].SetFloat("_PaintValue", Mathf.Lerp(paintValues[index], newPaintValue, normalizedT));
            }
            yield return new WaitForEndOfFrame();
        }

        for (int index = 0; index < materials.Length; index++)
        {
            materials[index].SetFloat("_PaintValue", newPaintValue);
        }
    }
}
