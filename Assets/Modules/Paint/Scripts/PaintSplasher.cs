﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintSplasher : MonoBehaviour
{

    [SerializeField]
    private float radius = 1f;

    public void Splash()
    {
        var colliders = Physics.OverlapSphere(transform.position, radius);
        foreach(var collider in colliders)
        {
            //Debug.Log(collider.name);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
