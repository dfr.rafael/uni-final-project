﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Color List", menuName = "Paint/Color List")]
public class ColorListData : ScriptableObject
{
    public List<Color> colors;
}
