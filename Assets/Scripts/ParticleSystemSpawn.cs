using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystemSpawn : MonoBehaviour
{

	[SerializeField]
	private GameObject prefab;

	[SerializeField]
	private float scale = 1.0f;

	public void Activate()
	{
		Activate( scale );
	}

	public void Activate(float scale)
	{
		GameObject go = Instantiate( prefab, transform.position, Quaternion.identity );
		var system = go.GetComponent<ParticleSystem>();
		var systemMain = system.main;
		systemMain.startSizeMultiplier = scale;
		system.Play();
	}
}
