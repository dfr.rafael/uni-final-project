﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[ExecuteAlways]
public class EnemiesManager : MonoBehaviour {

    public BezierAttributes bezierAttributes;

    [SerializeField]
    private List<Enemy> enemies = new List<Enemy>();

    public void Register(Enemy enemy) => enemies.Add(enemy);
    public void Unregister(Enemy enemy) => enemies.Remove(enemy);

    private void Awake()
    {
        enemies.Clear();
    }

    public void ClearAllEnemies(bool setHealthToMax = true)
    {
        enemies.ForEach((enemy) =>
        {
            if (setHealthToMax)
                enemy.Health.SetMax();
            else
                enemy.Health.SetMin();
        });
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        Vector3 offset = new Vector3(0f, bezierAttributes.linePower, 0f);

        Handles.zTest = UnityEngine.Rendering.CompareFunction.LessEqual;

        foreach (Enemy enemy in enemies)
        {
            Handles.DrawBezier(transform.position, enemy.transform.position,
                transform.position - offset, enemy.transform.position + offset,
                bezierAttributes.lineColor, new Texture2D(2, 2), bezierAttributes.lineWidth);
        }
    }
#endif

}
