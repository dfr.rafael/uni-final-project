using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class MaterialPropertyOverTime : MonoBehaviour
{

    [SerializeField]
    private float time;

	[SerializeField]
	private string propertyName;

    [SerializeField]
    private AnimationCurve curve;

	private Material material;

	private void Awake()
	{
		material = GetComponent<MeshRenderer>().material;
	}

	public void Activate()
	{
		StopCoroutine(ActivateEnumerator());
		StartCoroutine(ActivateEnumerator());
	}

    private IEnumerator ActivateEnumerator()
	{
		float current = 0;
		while (current < time)
		{
			float t = current / time;
			material.SetFloat(propertyName, curve.Evaluate(t));
			current += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}
		material.SetFloat(propertyName, curve.Evaluate(1f));
	}
}
