﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSceneManager : MonoBehaviour
{

	public void ChangeScene(int sceneIndex)
	{
		SceneManager.LoadScene( sceneIndex );
	}

	public void ResetScene(float delay = 0f)
	{
		StartCoroutine( DelayedSceneLoad( SceneManager.GetActiveScene().buildIndex, delay ) );
	}

	private IEnumerator DelayedSceneLoad(int sceneIndex, float delaySeconds)
	{
		yield return new WaitForSeconds( delaySeconds );
		SceneManager.LoadScene( sceneIndex );
	}

	public void QuitApplication()
	{
		Application.Quit();
	}
}
