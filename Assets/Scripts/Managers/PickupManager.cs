﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupManager : MonoBehaviour
{

    [SerializeField]
    private GameObject healthPickup;

    public void SpawnHealth(Vector3 position)
    {
        GameObject instantiated = Instantiate(healthPickup, position, Quaternion.identity, transform);
        instantiated.name = "Health Pickup";
    }
}
