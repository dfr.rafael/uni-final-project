﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CursorLocker : MonoBehaviour
{

    [SerializeField]
    private Key key = Key.C;

    private bool isVisible = false;

    void Start()
    {
        SetCursor(false);
    }

    void Update()
    {
        if ( Keyboard.current[key].wasPressedThisFrame )
        { 
            SetCursor(!isVisible);
        }
    }

    private void SetCursor(bool _isVisible)
    {
        isVisible = _isVisible;

        Cursor.lockState = !isVisible ? CursorLockMode.Locked : CursorLockMode.None;
        Cursor.visible = isVisible;
    }
}
