﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

[System.Serializable]
public class DebugInputAction {
    public Key key;
    public UnityEvent unityEvent;
}

public class DebugManager : MonoBehaviour
{
    [SerializeField]
    private List<DebugInputAction> inputActions;

	private void Update()
    {
        inputActions.ForEach(inputAction =>
        {
            if ( Keyboard.current[inputAction.key].wasPressedThisFrame) inputAction.unityEvent.Invoke();
        });
    }
}
