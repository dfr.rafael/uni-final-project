﻿using UnityEngine;

[CreateAssetMenu(fileName = "Number Freeze", menuName = "Number Modifiers")]
public class NumberFreeze : NumberModifier {
    public override int ModifyIncrease(int increaseValue, int currentValue)
    {
        return 0;
    }

    public override int ModifySet(int newValue, int currentValue)
    {
        return currentValue;
    }

	public override float Modify(float value)
	{
        return value;
	}
}
