﻿using UnityEngine;

public class NumberModifier : ScriptableObject {
    public virtual int ModifyIncrease(int increaseValue, int currentValue) { return increaseValue; }

    public virtual int ModifySet(int newValue, int currentValue) { return newValue; }

    public virtual float Modify(float value) { return value; }
}