using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Number Multiplier", menuName = "NumberModifier/NumberMultiplier")]
public class NumberMultiplier : NumberModifier {

	public float multiplier;

	public override float Modify(float number)
	{
		return number * multiplier;
	}

}
