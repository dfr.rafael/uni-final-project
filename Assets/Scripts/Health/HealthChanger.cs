﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthChanger : MonoBehaviour {

    [SerializeField]
    protected int value = 1;

    public int Value => value;
}
