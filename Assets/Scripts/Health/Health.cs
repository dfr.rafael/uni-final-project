﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using NaughtyAttributes;

public class Health : MonoBehaviour
{

	//
	// VALUES
	//

	[SerializeField]
	private int value;

	[SerializeField]
	private int maxValue;

	[SerializeField]
	private bool changeable = true;

	[SerializeField]
	private bool fixChangeableOnZero = false;

	[SerializeField]
	private bool fixChangeableOnMax = true;

	[SerializeField]
	private bool setToZeroOnStart = false;

	[SerializeField]
	private bool setToMaxOnStart = true;

	[SerializeField]
	private bool destroyOnZero = false;

	[SerializeField]
	private bool destroyOnMax = true;

	// Invokes method with absolute value
	[SerializeField]
	private IntEvent onChangeTotal;

	// Invokes method with normalized value [0-1]
	[SerializeField]
	private FloatEvent onChangeNormalized;

	// Invokes method with absolute difference.
	// First param is delta.
	// Second param is if it reached min (-1), max (1) or none (0).
	[SerializeField]
	private IntIntEvent onChangeDelta;

	[SerializeField]
	private UnityEvent onMaxValue;

	[SerializeField]
	private UnityEvent onZeroValue;

	private List<NumberModifier> healthModifiers = new List<NumberModifier>();

	//
	// PROPERTIES
	//

	public int Value { get => value; }
	public float NormalizedValue { get => Mathf.Clamp01( value / (float)maxValue ); }

	private void Start()
	{
		if ( setToZeroOnStart && setToMaxOnStart )
			Debug.LogWarning( "HEALTH :: Setting to ZERO and MAX at start." );

		if ( destroyOnZero && destroyOnMax )
			Debug.LogWarning( "HEALTH :: Destroying at ZERO and MAX." );

		if ( setToMaxOnStart )
		{
			value = maxValue;
			InvokeEvents();
			if ( fixChangeableOnZero )
			{
				onZeroValue.AddListener( () =>
					{
						changeable = false;
					}
				);
			}
		}

		if ( setToZeroOnStart )
		{
			value = 0;
			InvokeEvents();
			if ( fixChangeableOnMax )
			{
				onMaxValue.AddListener( () =>
				{
					changeable = false;
				}
			);
			}
		}

		if ( destroyOnZero ) onZeroValue.AddListener( () =>
		{
			Destroy( gameObject );
		} );

		if ( destroyOnMax ) onMaxValue.AddListener( () =>
		{
			Destroy( gameObject );
		} );
	}

	public void Increase(int increaseValue)
	{
		if ( !changeable ) return;

		ApplyIncreaseModifiers( ref increaseValue );

		value = Mathf.Clamp( value + increaseValue, 0, maxValue );

		InvokeEvents( increaseValue );
	}

	public void Set(int setValue)
	{
		if ( !changeable ) return;

		int oldValue = value;
		ApplySetModifiers( ref setValue );
		setValue = Mathf.Clamp( value, 0, maxValue );

		value = setValue;

		InvokeEvents( setValue - oldValue );
	}

	public void SetNormalized(float normalizedValue)
	{
		if ( !changeable ) return;

		if ( normalizedValue == 1 )
		{
			SetMax();
			return;
		}

		int oldValue = value;
		int newValue = Mathf.RoundToInt( Mathf.Lerp( 0f, maxValue, normalizedValue ) );
		ApplySetModifiers( ref newValue );

		value = Mathf.Clamp( newValue, 0, maxValue );
		InvokeEvents( newValue - oldValue );
	}

	public void SetMin()
	{
		if ( !changeable ) return;
		int oldValue = value;
		value = 0;
		InvokeEvents( 0 - oldValue );
	}

	public void SetMax()
	{
		if ( !changeable ) return;
		int oldValue = value;
		value = maxValue;
		InvokeEvents( maxValue - oldValue );
	}

	public void AddModifier(NumberModifier healthModifier)
	{
		healthModifiers.Add( healthModifier );
	}

	public void RemoveModifier(NumberModifier healthModifier)
	{
		healthModifiers.Remove( healthModifier );
	}

	private void InvokeEvents()
	{
		onChangeTotal.Invoke( value );
		onChangeNormalized.Invoke( NormalizedValue );

		if ( value == maxValue ) onMaxValue.Invoke();
		if ( value == 0 ) onZeroValue.Invoke();
	}

	private void InvokeEvents(int healthDelta)
	{
		onChangeDelta.Invoke( healthDelta, value == 0 ? -1 : value == maxValue ? 1 : 0 );
		InvokeEvents();
	}

	private void ApplyIncreaseModifiers(ref int increaseValue)
	{
		for ( int index = 0; index < healthModifiers.Count; index++ )
		{
			increaseValue = healthModifiers[index].ModifyIncrease( increaseValue, value );
		}
	}

	private void ApplySetModifiers(ref int setValue)
	{
		for ( int index = 0; index < healthModifiers.Count; index++ )
		{
			setValue = healthModifiers[index].ModifySet( setValue, value );
		}
	}

	public void AddOnMaxValue(UnityAction action)
	{
		onMaxValue.AddListener( action );
	}

	public void AddOnZeroValue(UnityAction action)
	{
		onZeroValue.AddListener( action );
	}

	//
	// DEBUG
	// 

	public bool showDebug = false;

	[ShowIf( "showDebug" ), SerializeField]
	private int debugDelta = -1;

	[ShowIf( "showDebug" ), Button]
	private void DebugChangeHealth()
	{
		Increase( debugDelta );
	}
}
