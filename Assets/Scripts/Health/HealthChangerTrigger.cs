﻿using UnityEngine;

[RequireComponent( typeof( Collider ), typeof( HealthChanger ) )]
public class HealthChangerTrigger : MonoBehaviour
{

	private HealthChanger changer;

	private void Start()
	{
		changer = GetComponent<HealthChanger>();
	}

	private void OnTriggerEnter(Collider collider)
	{
		var otherHealth = collider.gameObject.GetComponent<Health>(); ;

		if ( !otherHealth ) return;

		otherHealth.Increase( changer.Value );
	}
}
