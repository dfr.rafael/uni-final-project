using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthChangerArea : HealthChanger
{
	[Tooltip( "Health change for indirect hits" ),
	 SerializeField]
	private int indirectValue = 1;

	[SerializeField] private float radius = 1f;

	public void ApplyHealthChangeNoCollider()
	{
		var hits = Physics.SphereCastAll( transform.position, radius, Vector3.one );
		foreach ( var hit in hits )
		{
			hit.collider.GetComponent<Health>()?.Increase( value );
		}
	}

	public void ApplyHealthChange(GameObject collided = null)
	{
		var hits = Physics.SphereCastAll( transform.position, radius, Vector3.one );
		foreach ( var hit in hits )
		{
			hit.collider.GetComponent<Health>()?.Increase( collided == hit.collider.gameObject ? value : indirectValue );
		}
	}

	public void ApplyHealthChange(Collision collision)
	{
		ApplyHealthChange( collision.gameObject );
	}

	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.green;
		Gizmos.DrawWireSphere( transform.position, radius );
	}
}
