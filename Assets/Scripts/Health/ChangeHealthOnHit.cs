﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeHealthOnHit : MonoBehaviour
{

	[SerializeField]
	private int value = 1;

	private void OnCollisionEnter(Collision collision)
	{
		Health health = collision.gameObject.GetComponent<Health>();
		if ( health == null )
		{
			return;
		}

		health.Increase( value );
	}
}
