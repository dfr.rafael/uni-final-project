﻿using UnityEngine;

[RequireComponent(typeof(Collider), typeof(Health))]
public class HealthChangerCollider : MonoBehaviour
{

    private Health health;

    private void Awake()
    {
        health = GetComponent<Health>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        var healthChanger = collision.gameObject.GetComponent<HealthChanger>();

        if (!healthChanger) return;

        health.Increase(healthChanger.Value);
    }
}
