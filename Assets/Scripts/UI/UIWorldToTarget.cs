using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIWorldToTarget : MonoBehaviour
{

	[SerializeField] private bool setTargetToCamera = true;
	[SerializeField] private bool setTargetToPlayer = false;
	[SerializeField] private bool lockToAxisY = false;
	[SerializeField] private Transform target;


	private void Start()
	{
		if ( setTargetToCamera && setTargetToPlayer ) Debug.LogError( $"UI WORLD TO TARGET :: {name} :: Both camera and player targets up" );

		if ( setTargetToCamera )
			target = Camera.main.transform;
		else
			target = GameObject.FindGameObjectWithTag( "Player" ).transform;
	}

	private void Update()
	{
		Vector3 newRotation = Quaternion.LookRotation( (transform.position - target.position).normalized, Vector3.up ).eulerAngles;
		if ( lockToAxisY )
			newRotation.x = newRotation.z = 0.0f;
		transform.rotation = Quaternion.Euler( newRotation );
	}
}
