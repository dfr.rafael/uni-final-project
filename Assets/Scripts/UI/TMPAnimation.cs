using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using TMPro;

public class TMPAnimation : MonoBehaviour
{

	[SerializeField] private bool useFontSize = false;
	[SerializeField, Tooltip( "Multiples curve by initial size." )] private bool fontSizeRelative = false;
	[SerializeField, ShowIf( "useFontSize" )] private AnimationCurve fontSizeCurve = AnimationCurve.Linear( 0.0f, 0.0f, 1.0f, 1.0f );

	[SerializeField] private bool useAlpha = false;
	[SerializeField, ShowIf( "useAlpha" )] private AnimationCurve alphaCurve = AnimationCurve.Linear( 0.0f, 0.0f, 1.0f, 1.0f );

	[SerializeField] private bool useVelocity = false;
	[SerializeField, ShowIf( "useVelocity" )] private float baseVelocity = 1.0f;
	[SerializeField, ShowIf( "useVelocity" )] private AnimationCurve velocityCurveX = AnimationCurve.Linear( 0.0f, 0.0f, 1.0f, 1.0f );
	[SerializeField, ShowIf( "useVelocity" )] private AnimationCurve velocityCurveY = AnimationCurve.Linear( 0.0f, 0.0f, 1.0f, 1.0f );
	[SerializeField, ShowIf( "useVelocity" ), Tooltip( "Velocity in X can go both ways." )] private bool velocityBothX = false;
	[SerializeField, ShowIf( "useVelocity" ), Tooltip( "Velocity in Y can go both ways." )] private bool velocityBothY = false;


	private TextMeshProUGUI text;
	private float fontMultiplier;

	private Vector3 initialPosition;
	private int velocitySignX = 1;
	private int velocitySignY = 1;

	void Start()
	{
		text = GetComponent<TextMeshProUGUI>();

		fontMultiplier = fontSizeRelative ? text.fontSize : 1.0f;
		initialPosition = transform.position;
	}

	/// <summary>
	/// Returns -1 or 1.
	/// </summary>
	/// <returns></returns>
	private int RandOneMinusOne()
	{
		return Mathf.RoundToInt( (Random.Range( 0, 2 ) - 0.5f) * 2.0f );
	}

	public void Reset()
	{
		transform.position = initialPosition;
		velocitySignX = 1;
		velocitySignY = 1;
	}

	public void Randomize()
	{
		if ( useVelocity )
		{
			velocitySignX = velocityBothX ? RandOneMinusOne() : 1;
			velocitySignY = velocityBothY ? RandOneMinusOne() : 1;
		}
	}

	public void Animate(float value)
	{
		if ( useAlpha )
		{
			text.fontSize = fontSizeCurve.Evaluate( value ) * fontMultiplier;
		}

		if ( useAlpha )
			text.alpha = alphaCurve.Evaluate( value );

		if ( useVelocity )
		{
			transform.position += new Vector3(
				velocityCurveX.Evaluate( value ) * baseVelocity * velocitySignX,
				velocityCurveY.Evaluate( value ) * baseVelocity * velocitySignY,
				0.0f );
		}

	}
}
