using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimationEvent : MonoBehaviour
{

	[SerializeField] private float duration = 1f;
	[SerializeField] private bool runOnStart = false;

	[SerializeField] private UnityEvent onStart;
	[SerializeField] private UnityEvent<float> onAnimate;
	[SerializeField] private UnityEvent onEnd;

	private Coroutine coroutine;

	private void Start()
	{
		if ( duration <= 0.0f )
			Debug.LogError( "ANIMATION EVENT :: Duration should be highter than ZERO." );

		if ( runOnStart )
			Activate();
	}

	[NaughtyAttributes.Button]
	public void Activate()
	{
		coroutine = StartCoroutine( RunAnimation() );
	}

	public void Stop()
	{
		if ( coroutine != null )
			StopCoroutine( coroutine );

		if ( onEnd != null )
			onEnd.Invoke();
	}

	private IEnumerator RunAnimation()
	{
		if ( onStart != null )
			onStart.Invoke();

		if ( onAnimate == null )
		{
			yield return new WaitForSeconds( duration );
		}
		else
		{
			float t = 0.0f;
			while ( t < duration )
			{
				onAnimate.Invoke( t / duration );
				t += Time.deltaTime;
				yield return new WaitForEndOfFrame();
			}
		}

		if ( onEnd != null )
			onEnd.Invoke();
	}
}
