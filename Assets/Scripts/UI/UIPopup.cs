using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPopup : MonoBehaviour
{

	[SerializeField] private Vector3 offset;
	[SerializeField] private bool multipleChoices = false;
	[SerializeField] private GameObject prefab;
	[SerializeField, NaughtyAttributes.ShowIf( "multipleChoices" )] private GameObject prefabMin;
	[SerializeField, NaughtyAttributes.ShowIf( "multipleChoices" )] private GameObject prefabMax;

	public void Activate()
	{
		Instantiate( prefab, transform.position + offset, Quaternion.identity );
	}

	public void Activate(int value, int choice)
	{
		GameObject chosenPrefab = prefab;
		if ( multipleChoices )
		{
			if ( choice == -1 ) chosenPrefab = prefabMin;
			if ( choice == 1 ) chosenPrefab = prefabMax;
		}

		GameObject popup = Instantiate( chosenPrefab, transform.position + offset, Quaternion.identity );

		TMPro.TextMeshProUGUI textMesh = popup.GetComponentInChildren<TMPro.TextMeshProUGUI>();
		if ( textMesh != null )
			textMesh.text = value.ToString();
	}

	public void ActivateNormalized(float value)
	{
		int transformedValue = Mathf.RoundToInt( value * 100.0f );
		int choice = value == 0.0f ? -1 : value == 1.0f ? 1 : 0;
		Activate( transformedValue, choice );
	}

#if UNITY_EDITOR
	public void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.blue;
		Gizmos.DrawCube( transform.position + offset, new Vector3( 0.1f, 0.1f, 0.1f ) );
	}
#endif
}
