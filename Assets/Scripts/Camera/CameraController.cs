using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraController : MonoBehaviour
{

    [SerializeField]
    private int currentZoom = 0;

    public int CurrentZoom
	{
		get
		{
            return currentZoom;
		}
		set
		{
            currentZoom = Mathf.Clamp(value, 0, zoomLevels.Count-1);
            SetZoom();
		}
	}

    [SerializeField]
    private List<float> zoomLevels = new List<float>() { 10f, 8f, 7f};

    [SerializeField]
    private CinemachineVirtualCamera virtualCamera;

    void Start()
    {
        if(!virtualCamera )
		{
            Debug.LogError("Camera controller's virtual camera is not set!");
		}
    }

	private void OnValidate()
	{
        CurrentZoom = currentZoom;
    }

	private void SetZoom()
	{
        virtualCamera.m_Lens.OrthographicSize = zoomLevels[currentZoom];
	}


}
