﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class FirstPersonCamera : MonoBehaviour
{

	[SerializeField]
	private Vector2 mouseSensitivity = new Vector2( 20f, 20f );

	[SerializeField]
	private Vector2 controllerSensitivity = new Vector2( 1000f, 1000f );

	[SerializeField]
	private float smoothness = 1f;

	private float pitch = 0;
	private float yaw = 0;

	private float currentPitch = 0;
	private float currentYaw = 0;

	private Vector2 input = Vector2.zero;

	void Update()
	{
		yaw += input.x;
		pitch += input.y;
		// Clamping Pitch
		pitch = Mathf.Clamp( pitch, -90f, 90f );

		currentPitch = Mathf.Lerp( currentPitch, pitch, smoothness * Time.deltaTime );
		currentYaw = Mathf.Lerp( currentYaw, yaw, smoothness * Time.deltaTime );

		// Clamping Yaw
		if ( currentYaw > 360.0f )
		{
			currentYaw -= 360.0f;
			yaw -= 360.0f;
		}
		if ( currentYaw < 360.0f )
		{
			currentYaw += 360.0f;
			yaw += 360.0f;
		}

		transform.localRotation = Quaternion.Euler( currentPitch, currentYaw, 0f );
	}

	public void OnLook(InputAction.CallbackContext context)
	{
		Vector2 sensitivity = context.control.device == Mouse.current
			? mouseSensitivity
			: controllerSensitivity;
		input = context.ReadValue<Vector2>() * sensitivity * Time.deltaTime;
	}
}
