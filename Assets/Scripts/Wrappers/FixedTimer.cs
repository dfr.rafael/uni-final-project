﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedTimer : CustomYieldInstruction {
    public override bool keepWaiting
    {
        get
        {
            return false;
        }
    }

    public FixedTimer(int frames)
    {

    }

    public FixedTimer(float seconds)
    {

    }
}
