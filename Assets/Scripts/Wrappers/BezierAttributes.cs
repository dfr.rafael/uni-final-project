﻿using UnityEngine;

[System.Serializable]
public class BezierAttributes
{
    public Color lineColor = Color.red;

    [Range(0.0001f, 10f)]
    public float linePower = 1f;

    [Range(0.0001f, 10f)]
    public float lineWidth = 1f;
}
