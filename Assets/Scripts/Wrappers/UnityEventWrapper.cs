﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class UnityEventWrapper : UnityEvent { }

[System.Serializable]
public class IntEvent : UnityEvent<int> { }

[System.Serializable]
public class FloatEvent : UnityEvent<float> { }

[System.Serializable]
public class IntIntEvent : UnityEvent<int, int> { }

[System.Serializable]
public class CollisionEvent : UnityEvent<Collision> { }


[System.Serializable]
public class ColliderEvent : UnityEvent<Collider> { }

[System.Serializable]
public class GameObjectEvent : UnityEvent<GameObject> { }

[System.Serializable]
public class Vector3Event : UnityEvent<Vector3> { }