﻿using UnityEngine;
using UnityEngine.Events;

public static class ExtensionMethods {

    public static float Remap(this float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

    public static void For(this int value, UnityAction action)
    {
        for (int i = 0; i < value; i++)
        {
            action.Invoke();
        }
    }

    public static float Random(this UnityEngine.Vector2 value)
    {
        return UnityEngine.Random.Range(value.x, value.y);
    }

}
