﻿using System;
using UnityEngine;

public class AbstractData<T> : ScriptableObject {
	public T _value;

	public T Value { get => _value; set => _value = value; }
}

[CreateAssetMenu(fileName = "Int Data", menuName = "Primitive Data/Int Data")]
public sealed class IntData : AbstractData<int> {

	public static implicit operator int(IntData data)
	{
		return data.Value;
	}

	public static int operator +(IntData data, int value)
	{
		return data.Value + value;
	}

	public static int operator -(IntData data, int value)
	{
		return data.Value - value;
	}
}

[CreateAssetMenu(fileName = "Float Data", menuName = "Primitive Data/Float Data")]
public class FloatData : AbstractData<float> {
	public static implicit operator float(FloatData data)
	{
		return data.Value;
	}

	public static float operator +(FloatData data, float value)
	{
		return data.Value + value;
	}

	public static float operator -(FloatData data, float value)
	{
		return data.Value - value;
	}
}