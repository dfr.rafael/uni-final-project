﻿using UnityEngine;

[RequireComponent(typeof(Collider))]
public class DestroyOnHit : MonoBehaviour
{

    [SerializeField]
    private bool onCollision = true;

    [SerializeField]
    private bool onTrigger = true;

    private void OnCollisionEnter(Collision collision)
    {
        if (!onCollision) return;
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (!onTrigger) return;
        Destroy(gameObject);
    }
}
