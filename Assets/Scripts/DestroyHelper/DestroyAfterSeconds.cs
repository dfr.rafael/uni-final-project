﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterSeconds : MonoBehaviour
{
    [SerializeField]
    private float secondsToDestroy = 1f;

    [SerializeField]
    private bool activateOnAwake = true;

    private void Awake()
    {
        if (activateOnAwake) Activate();
    }

    public void Activate()
    {
        Destroy(gameObject, secondsToDestroy);
    }

    public void ActivateImmediate()
	{
        Destroy(gameObject);
	}
}
