﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnDestroyEventGameObject : MonoBehaviour
{
    [SerializeField]
    private GameObjectEvent onDestroy;

    private void OnDestroy()
    {
        onDestroy.Invoke(gameObject);
    }
}
