﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnDestroyEventVec3 : MonoBehaviour
{
    [SerializeField]
    private Vector3Event onDestroy;

    private void OnDestroy()
    {
        onDestroy.Invoke(transform.position);
    }
}
