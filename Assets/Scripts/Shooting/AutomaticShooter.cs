﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(IShooter))]
public class AutomaticShooter : MonoBehaviour {

	[SerializeField]
	private float minDistance = 1f;

	private Transform playerTransform;
	private IShooter shooter;

	private void Awake()
	{
		shooter = GetComponent<IShooter>();
		playerTransform = GameObject.FindGameObjectWithTag("Player").transform; //TODO: Fix please!!!
	}

	private void Update()
	{
		float distance = Vector3.Distance(transform.position, playerTransform.position);
		if (distance <= minDistance)
			shooter.Shoot();
	}

	private void OnDrawGizmos()
	{
		Gizmos.color = Color.green;
		Gizmos.DrawWireSphere(transform.position, minDistance);
	}
}
