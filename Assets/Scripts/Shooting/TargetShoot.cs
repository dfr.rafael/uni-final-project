using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetShoot : DirectionalShoot
{
    [SerializeField]
    private Transform target;

    protected new void Update()
    {
        if (Input.GetMouseButton(0))
        {
            currentPrefab = shootPrefab;
            isAlt = false;
            Shoot();
            SetIsFiring(true);
        }
        else if (Input.GetMouseButton(1))
        {
            currentPrefab = shootPrefabAlt;
            isAlt = true;
            Shoot();
            SetIsFiring(true);
        }
        else
        {
            SetIsFiring(false);
        }
    }

    public new void Shoot()
	{
        if (!canShoot) return;

        GameObject newShoot = Instantiate(currentPrefab, shootSpawn.position, Quaternion.LookRotation(target.position - transform.position, Vector3.up));
        newShoot.name = "Shoot";

        onShoot?.Invoke();

        StartCoroutine(ShootCooldown(isAlt));
    }
}
