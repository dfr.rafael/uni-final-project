﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraBasedShoot : DirectionalShoot
{

    [SerializeField]
    private Transform target;

    private new void Update()
    {
        if ( Mouse.current.leftButton.wasPressedThisFrame )
        {
            currentPrefab = shootPrefab;
            isAlt = false;
            ShootDirectional();
        }

        if ( Mouse.current.rightButton.wasPressedThisFrame )
        {
            currentPrefab = shootPrefabAlt;
            isAlt = true;
            ShootDirectional();
        }
    }

    public void ShootDirectional()
    {
        if (!canShoot) return;

        Quaternion shootRotation = Quaternion.LookRotation(target.position - transform.position, Vector3.up);
        GameObject newShoot = Instantiate(currentPrefab, shootSpawn.position, shootRotation);
        newShoot.name = "Shoot";

        onShoot?.Invoke();

        StartCoroutine(ShootCooldown(isAlt));
    }
}
