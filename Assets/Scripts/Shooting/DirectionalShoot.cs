﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DirectionalShoot : MonoBehaviour, IShooter {

    // Exposed
    [SerializeField]
    protected float shootsPerSecond = 6;

    [SerializeField]
    protected float shootsPerSecondAlt = 2;

    [SerializeField]
    protected GameObject shootPrefab;

    [SerializeField]
    protected GameObject shootPrefabAlt;

    [SerializeField]
    protected Transform shootSpawn;

    [SerializeField]
    protected UnityEvent onShoot;

    [SerializeField]
    protected bool isControllable = false;

    // Internal
    protected bool canShoot = true;

    protected GameObject currentPrefab;

    protected bool isAlt = false;

    [SerializeField]
    private UnityEvent onFiring;

    [SerializeField]
    private UnityEvent onStopFiring;

    private bool isFiring = false;

    private void Start()
	{
        currentPrefab = shootPrefab;
    }

	protected void Update()
    {
        if (!isControllable) return;

        if (Input.GetMouseButton(0))
        {
            currentPrefab = shootPrefab;
            isAlt = false;
            Shoot();
            SetIsFiring(true);
        }
        else if (Input.GetMouseButton(1))
        {
            currentPrefab = shootPrefabAlt;
            isAlt = true;
            Shoot();
            SetIsFiring(true);
		}
		else
		{
            SetIsFiring(false);
        }


    }

    protected void SetIsFiring(bool _isFiring)
	{
		if (!isFiring && _isFiring)
		{
            isFiring = true;
            onFiring.Invoke();
        }else if(isFiring && !_isFiring)
		{
            isFiring = false;
            onStopFiring.Invoke();
        }
	}

    public void Shoot()
    {
        if (!canShoot) return;

        GameObject newShoot = Instantiate(currentPrefab, shootSpawn.position, transform.rotation);
        newShoot.name = "Shoot";

        onShoot?.Invoke();

        StartCoroutine(ShootCooldown(isAlt));
    }

    protected IEnumerator ShootCooldown(bool isAlt)
    {
        canShoot = false;
        
        float secondsPerShoot = 1 / (!isAlt ? shootsPerSecond : shootsPerSecondAlt);
        float counter = 0;
        while (counter < secondsPerShoot)
        {
            yield return new WaitForFixedUpdate();
            counter += Time.fixedDeltaTime;
        }

        canShoot = true;
    }
}
