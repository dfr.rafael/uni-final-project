using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(DirectionalMovement), typeof(Animator))]
public class MovementAnimatorParameters : MonoBehaviour
{

    [SerializeField]
    private string parameter;

    [SerializeField]
    private float transitionTime = 0.1f;
    private float smoothVelocity = 0.0f;

    private float currentVelocity = 0.0f;
    private float targetVelocity;

    private Animator animator;
    private DirectionalMovement movement;

    void Awake()
    {
        animator = GetComponent<Animator>();
        movement = GetComponent<DirectionalMovement>();
    }

    void Update()
    {
        targetVelocity = movement.InputVelocity;
        currentVelocity = Mathf.SmoothDamp(currentVelocity, targetVelocity, ref smoothVelocity, transitionTime);
        animator.SetFloat(parameter, currentVelocity);
    }
}
