﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Jump : MonoBehaviour
{

    [SerializeField]
    private float force = 10.0f;

    [SerializeField]
    private float gravityModifier = 10.0f;

    [SerializeField]
    private KeyCode key;

    [SerializeField]
    private Vector3 rayOffset = Vector3.zero;

    [SerializeField]
    private float raySpacing = 0.5f;

    [SerializeField]
    private float rayDistance = 0.5f;

    [SerializeField]
    private LayerMask mask;

    private Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (Input.GetKeyDown(key))
        {
            JumpMethod();
        }
    }

    private bool IsGrounded()
    {
        Vector3 currentOffset;
        for (int x = -1; x < 1; x++)
        {
            for (int y = -1; y < 1; y++)
            {
                currentOffset = new Vector3(x, 0, y) * raySpacing;
                currentOffset += rayOffset;

                if(Physics.Raycast(transform.position + currentOffset, -Vector3.up, rayDistance, mask))
                {
                    return true;
                }
            }
        }
        return false;
    }

    private void JumpMethod()
    {
        if (IsGrounded())
        {
            rb.AddForce(Vector3.up * force, ForceMode.VelocityChange);
        }
    }

    private void FixedUpdate()
    {
        if (!IsGrounded())
        {
            rb.AddForce(-Vector3.up * gravityModifier, ForceMode.Acceleration);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Vector3 currentOffset;
        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                currentOffset = new Vector3(x, 0, y) * raySpacing;
                currentOffset += rayOffset;

                Gizmos.DrawLine(transform.position + currentOffset, transform.position + currentOffset - Vector3.up * rayDistance);
            }
        }
    }
}
