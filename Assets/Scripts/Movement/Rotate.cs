﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    [SerializeField]
    private Vector3 rotationsPerSecond;

    private Vector3 speed;

    private void Awake()
    {
        speed = rotationsPerSecond * 180.0f;
    }

    private void Update()
    {
        Vector3 pivot = transform.position;
        transform.RotateAround(pivot, Vector3.right, speed.x * Time.deltaTime);
        transform.RotateAround(pivot, Vector3.up, speed.y * Time.deltaTime);
        transform.RotateAround(pivot, Vector3.forward, speed.z * Time.deltaTime);
    }
}
