﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBasedMovement : DirectionalMovement {

    [SerializeField]
    private float turnSpeed = 10f;

    private Transform cameraTransform;

    protected override void Awake()
    {
        base.Awake();
        cameraTransform = Camera.main.transform;
    }

    protected override void Update()
    {
        Vector3 cameraForward = Vector3.Cross(cameraTransform.right, Vector3.up);

        direction = transform.right * input.x;
        direction += cameraForward * input.y;
        direction.Normalize();

        if (input != Vector2.zero)
        {
            facingDirection = input;
        }

        UpdateRotation();
    }

    private void UpdateRotation()
    {
        Vector3 newRotation = new Vector3(0, cameraTransform.eulerAngles.y, 0);
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(newRotation), turnSpeed * Time.deltaTime);
    }
}
