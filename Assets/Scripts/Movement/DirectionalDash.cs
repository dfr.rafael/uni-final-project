﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(DirectionalMovement))]
public class DirectionalDash : MonoBehaviour {

    // Editor
    [SerializeField]
    private float distance = 5.0f;

    [SerializeField]
    private float duration = 0.5f;

    [SerializeField]
    private float dashCooldownDuration = 1.0f;

    [SerializeField]
    private KeyCode key = KeyCode.LeftControl;

    // Auxiliary
    private bool canDash = true;

    // References
    private DirectionalMovement movement;
    private Rigidbody rb;

    [SerializeField]
    private UnityEvent<Vector3> onDash;

    private void Awake()
    {
        movement = GetComponent<DirectionalMovement>();
        rb = GetComponent<Rigidbody>();
    }

    public void OnDash()
	{
        StopCoroutine( EnqueueDash() );
        StartCoroutine( EnqueueDash() );
    }

    private IEnumerator EnqueueDash()
    {
        if (!canDash)
            yield return new WaitForEndOfFrame();
        else
        {
            Dash();
        }
    }

    private IEnumerator DashCooldown()
    {
        canDash = false;
        yield return new WaitForSeconds(dashCooldownDuration);
        canDash = true;
    }

    private void Dash()
    {
        rb.AddForce(movement.Direction * Time.fixedDeltaTime * distance, ForceMode.VelocityChange);
        onDash.Invoke(transform.position);
        StartCoroutine(DashCooldown());
    }
}
