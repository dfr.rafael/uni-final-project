﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class SpawnImpulse : MonoBehaviour
{

    [SerializeField]
    private Vector3 force = new Vector3(0.0f, 0.0f, 0.0f);

    [SerializeField]
    private ForceMode forceMode = ForceMode.Impulse;

    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.AddForce(transform.TransformDirection(force), forceMode);
    }

    public void Scale(Vector3 scale)
	{
        force.Scale(scale);
	}
}
