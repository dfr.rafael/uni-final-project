﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(DirectionalMovement))]
public class DirectionalLookForward : MonoBehaviour
{

    private DirectionalMovement directionalMovement;

    private void Awake()
    {
        directionalMovement = GetComponent<DirectionalMovement>();
    }

    void Update()
    {
        Vector3 facingDirection = directionalMovement.FacingDirection;
        float rotationAngle = Vector2.SignedAngle(Vector2.up, new Vector2(-facingDirection.x, facingDirection.y));
        transform.rotation = Quaternion.Euler(0f, rotationAngle, 0f);
    }
}
