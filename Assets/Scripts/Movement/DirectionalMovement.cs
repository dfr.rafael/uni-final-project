﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class DirectionalMovement : MonoBehaviour
{
	// Editor fields
	[SerializeField]
	protected float speed = 1.0f;

	[SerializeField]
	protected float slowSpeedFactor = 0.5f;

	[SerializeField]
	protected float drag = 0.5f;

	[SerializeField]
	protected bool isDiagonal = false;

	// References
	protected Rigidbody rb;

	// Auxiliary
	protected Vector2 input;
	protected Vector3 direction;
	protected Vector2 facingDirection = new Vector2( 0f, 1f );

	public bool IsSlow = false;

	public Vector3 Direction => direction;
	public Vector2 FacingDirection => facingDirection;
	public float InputVelocity => input.normalized.magnitude;

	protected virtual void Awake()
	{
		rb = GetComponent<Rigidbody>();
	}

	protected virtual void Update()
	{
		direction = new Vector3( input.x, 0f, input.y ).normalized;

		if ( input != Vector2.zero )
		{
			facingDirection = input;
		}
	}

	protected virtual void FixedUpdate()
	{
		if ( direction.sqrMagnitude > 0 )
		{
			rb.AddForce( direction * GetCurrentSpeed() * Time.fixedDeltaTime, ForceMode.VelocityChange );
		}
		Vector3 planarVelocity = new Vector3( rb.velocity.x, 0, rb.velocity.z );
		rb.AddForce( -planarVelocity * drag * Time.fixedDeltaTime, ForceMode.Impulse );
	}

	// INPUT SYSTEM
	public void OnMove(InputAction.CallbackContext context)
	{
		input = context.ReadValue<Vector2>();
	}

	private float GetCurrentSpeed()
	{
		return speed * (IsSlow ? slowSpeedFactor : 1.0f);
	}

	public void SetIsSlow(bool isSlow)
	{
		this.IsSlow = isSlow;
	}
}
