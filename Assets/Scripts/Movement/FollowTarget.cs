﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{

    [SerializeField]
    private Transform target;

    private Vector3 initialOffset = Vector3.zero;

    void Start()
    {
        initialOffset = transform.position - target.position;
    }

    void LateUpdate()
    {
        transform.position = target.position + initialOffset;
    }
}
