﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class DirectionalMovementAnimation : DirectionalMovement {

    private Animator animator;

    private new void Awake()
    {
        base.Awake();
        animator = GetComponent<Animator>();
    }

    override protected void Update()
    {
        base.Update();

        if (input != Vector2.zero)
        {
            animator.SetFloat("velocity", 1.0f);
        }
        else
        {
            animator.SetFloat("velocity", 0.0f);
        }
    }

    override protected void FixedUpdate()
	{
        base.FixedUpdate();
	}

}
