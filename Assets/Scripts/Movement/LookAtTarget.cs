using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LookAtTarget : MonoBehaviour
{

	[SerializeField]
	private Transform target;

	[SerializeField]
	private bool onlyY = false;

	[SerializeField, Tooltip( "Min distance for tracking to work. Use -1 for infinity." )]
	private float minDistance = -1f;

	[SerializeField]
	private bool lineOfSight = true;

	[SerializeField, NaughtyAttributes.ShowIf( "lineOfSight" )]
	private LayerMask layer;

	[SerializeField, Tooltip( "Maximum turning speed in deg/s." )]
	private float angularSpeed = 120f;

	[SerializeField]
	private UnityEvent onSight;

	[SerializeField]
	private UnityEvent onLoseSight;

	[SerializeField]
	private Vector3 upVector = Vector3.up;
	[SerializeField]
	private bool rotateAround = false;
	[SerializeField, NaughtyAttributes.ShowIf( "rotateAround" )]
	public Vector3 rotateVector = Vector3.up;
	[SerializeField, NaughtyAttributes.ShowIf( "rotateAround" )]
	public float rotateDegrees = 0.0f;

	// CACHED VALUES
	private float minDistanceSquared = Mathf.Infinity;
	private const float minAngle = 0.01f;
	private bool isOnSight = false;
	private Matrix4x4 localToWorld;

	private Vector3 ToTarget
	{
		get
		{
			return target.position - transform.position;
		}
	}

	void Start()
	{
		if ( target == null )
			Debug.LogError( "LOOK AT TARGET :: No target found." );

		if ( minDistance > 0f )
			minDistanceSquared = minDistance * minDistance;

		localToWorld = transform.localToWorldMatrix;

		onSight.AddListener( () => { isOnSight = true; } );
		onLoseSight.AddListener( () => { isOnSight = false; } );
	}

	void FixedUpdate()
	{
		if ( !CheckDistance() )
		{
			if ( isOnSight )
				onLoseSight.Invoke();
			return;
		}


		if ( lineOfSight )
		{
			if ( !CheckLineOfSight() )
			{
				if ( isOnSight )
					onLoseSight.Invoke();
				return;
			}
		}

		if ( !isOnSight )
		{
			onSight.Invoke();
		}

		Vector3 toTarget = ToTarget;
		if ( onlyY )
			toTarget.y = transform.position.y;
		toTarget.Normalize();

		Quaternion rotationToTarget = Quaternion.LookRotation( toTarget, localToWorld.MultiplyVector( upVector.normalized ).normalized );
		if ( !CheckAngle( rotationToTarget ) ) return;

		if ( rotateAround )
			rotationToTarget = rotationToTarget * Quaternion.AngleAxis( rotateDegrees, localToWorld.MultiplyVector( rotateVector.normalized ).normalized );

		transform.rotation = Quaternion.RotateTowards( transform.rotation, rotationToTarget, Time.deltaTime * angularSpeed );
	}

	private bool CheckLineOfSight()
	{
		if ( Physics.Raycast( transform.position, target.position - transform.position, out RaycastHit hit, minDistance, layer.value ) )
		{
			return hit.collider.gameObject == gameObject || hit.collider.gameObject == target.gameObject;
		}
		return true;
	}

	private bool CheckDistance()
	{
		return minDistanceSquared == Mathf.Infinity
			? true
			: Vector3.SqrMagnitude( ToTarget ) <= minDistanceSquared;
	}

	private bool CheckAngle(Quaternion rotation)
	{
		return Quaternion.Angle( transform.rotation, rotation ) > minAngle;
	}

	private void SetTarget(Transform newTarget)
	{
		target = newTarget;
	}

#if UNITY_EDITOR
	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere( transform.position, minDistance );

		Gizmos.color = Color.blue;
		Gizmos.DrawLine( transform.position, transform.position + localToWorld.MultiplyVector( upVector.normalized ).normalized * 5.0f );

		Gizmos.color = Color.green;
		Gizmos.DrawLine( transform.position, transform.position + transform.localToWorldMatrix.MultiplyVector( upVector.normalized ).normalized * 5.0f );

		Gizmos.color = Color.white;
		Gizmos.DrawLine( transform.position, transform.position + transform.forward * 5.0f );

		Vector3 toTarget = ToTarget;
		if ( onlyY )
			toTarget.y = transform.position.y;
		toTarget.Normalize();
		Gizmos.color = Color.black;
		Gizmos.DrawLine( transform.position, transform.position + toTarget );
	}
#endif
}
