﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnTriggerEvents : MonoBehaviour
{

    [SerializeField]
    private UnityEvent onTrigger;

    [SerializeField]
    private ColliderEvent onTriggerWithParameter;

    [SerializeField]
    private List<LayerMaskEvent> layerMaskEvents;

    private void OnTriggerEnter(Collider other)
    {
        onTrigger.Invoke();
        onTriggerWithParameter.Invoke(other);

        int colliderMask = other.gameObject.layer;
        foreach ( var layerMaskEvent in layerMaskEvents )
        {
            int eventMask = layerMaskEvent.mask.value;
            if ( (eventMask & 1 << colliderMask) != 0 )
            {
                layerMaskEvent.action.Invoke();
                layerMaskEvent.actionWithObject.Invoke(other.gameObject);
            }
        }
    }
}
