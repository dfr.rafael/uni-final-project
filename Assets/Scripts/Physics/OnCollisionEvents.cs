﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class LayerMaskEvent
{
	public LayerMask mask;
	public UnityEvent action;
	public UnityEvent<GameObject> actionWithObject;
}

public class OnCollisionEvents : MonoBehaviour
{

	[SerializeField]
	private UnityEvent onCollision;

	[SerializeField]
	private CollisionEvent onCollisionWithParameter;

	[SerializeField]
	private List<LayerMaskEvent> layerMaskEvents;

	private void OnCollisionEnter(Collision collision)
	{
		onCollision.Invoke();
		onCollisionWithParameter.Invoke( collision );

		int colliderMask = collision.gameObject.layer;
		foreach ( var layerMaskEvent in layerMaskEvents )
		{
			int eventMask = layerMaskEvent.mask.value;
			if ( (eventMask & 1 << colliderMask) != 0 )
			{
				layerMaskEvent.action.Invoke();
				layerMaskEvent.actionWithObject.Invoke( collision.gameObject );
			}
		}
	}
}
