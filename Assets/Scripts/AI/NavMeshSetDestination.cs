using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent( typeof( NavMeshAgent ) )]
public class NavMeshSetDestination : MonoBehaviour
{

	public Vector3 offset;
	public Transform target;
	public float sensorRadius = 10.0f;
	public LayerMask mask;
	public float stepSize = 1.0f;

	public bool moveRight = true;
	private NavMeshAgent agent;
	private Vector3 newDestination;

	void Start()
	{
		agent = GetComponent<NavMeshAgent>();
	}

	private void Update()
	{
		Vector3 toTarget = target.position - transform.position;
		toTarget.y = transform.position.y;
		toTarget.Normalize();

		var ray = new Ray( transform.position, toTarget );
		Debug.DrawLine( transform.position, transform.position + toTarget * sensorRadius, Color.yellow );

		if ( Physics.Raycast( ray, out RaycastHit hit, sensorRadius, mask.value ) )
		{
			Debug.Log("Hit");
			Vector3 hitToT = (transform.position - hit.point).normalized;
			Vector3 cross = Vector3.Cross( hitToT, moveRight ? Vector3.up : Vector3.down );

			Debug.DrawLine( transform.position, hit.point, Color.red );
			Debug.DrawLine( hit.point, hit.point + cross, Color.green );
			Debug.DrawLine( hit.point, hit.point + hitToT, Color.blue );

			//Quaternion direction = Quaternion.LookRotation( (hit.point - transform.position), Vector3.up );
			//direction *= Quaternion.AngleAxis( 90.0f, Vector3.up );

			Quaternion direction = Quaternion.LookRotation( cross, Vector3.up );
			newDestination = transform.position + direction * Vector3.forward * stepSize;

			agent.SetDestination( newDestination );
		}
		//else
		//{
		//	newDestination = transform.position + transform.forward * stepSize;
		//	agent.SetDestination( newDestination );
		//}
	}

	[NaughtyAttributes.Button]
	private void SetDestination()
	{
		agent.SetDestination( transform.position + transform.TransformDirection( offset ) );
	}

	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere( transform.position, sensorRadius );

		Gizmos.color = Color.green;
		Gizmos.DrawWireSphere( newDestination, 0.5f );
		//Gizmos.DrawLine( transform.position, transform.position + transform.TransformDirection( offset ) );
	}
}
