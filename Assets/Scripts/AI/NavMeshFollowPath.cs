using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent( typeof( NavMeshAgent ) )]
public class NavMeshFollowPath : MonoBehaviour
{

	public List<Vector3> path = new List<Vector3>();

	[SerializeField, Tooltip( "Seconds between each path recalculation." )]
	private float pathUpdateSeconds = 0.25f;

	[SerializeField, Tooltip( "Minimum distance before moving to next node." )]
	private float minNodeDistance = 0.01f;

	[SerializeField]
	private bool randomizePathList = false;

	private NavMeshAgent agent;
	private Coroutine updatePathCoroutine;
	private int currentIndex = 0;

	private void Awake()
	{
		agent = GetComponent<NavMeshAgent>();
	}

	private void OnEnable()
	{
		MoveToCloserNode();
		updatePathCoroutine = StartCoroutine( UpdatePath() );
	}

	private void OnDisable()
	{
		if ( updatePathCoroutine != null )
			StopCoroutine( updatePathCoroutine );
	}

	private void MoveToCloserNode()
	{
		float minDist = Mathf.Infinity;
		int minIdx = 0;

		for ( int i = 0; i < path.Count; i++ )
		{
			float nodeDist = Vector3.SqrMagnitude( path[i] - transform.position );
			if ( nodeDist < minDist )
			{
				minDist = nodeDist;
				minIdx = i;
			}
		}

		agent.destination = path[minIdx];
	}

	private IEnumerator UpdatePath()
	{
		while ( true )
		{
			if ( !agent.enabled ) break;

			if ( agent.remainingDistance <= minNodeDistance + agent.stoppingDistance )
			{
				if ( randomizePathList )
				{
					currentIndex = Random.Range(0, path.Count);
				}
				else {
					currentIndex = (currentIndex + 1) % path.Count; // Clamping index to [0, path.Count - 1]
				}
				agent.destination = path[currentIndex];
			}

			yield return new WaitForSeconds( pathUpdateSeconds );

		}
	}

}
