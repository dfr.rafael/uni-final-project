﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(NavMeshFollowTarget))]
public class SetFollowTargetToPlayer : MonoBehaviour
{
    private void Awake()
    {
        Transform playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        GetComponent<NavMeshFollowTarget>().SetTarget(playerTransform);
    }
}
