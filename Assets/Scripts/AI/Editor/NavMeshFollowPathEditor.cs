using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor( typeof( NavMeshFollowPath ) )]
public class NavMeshFollowPathEditor : Editor
{
	private void OnSceneGUI()
	{
		NavMeshFollowPath component = target as NavMeshFollowPath;

		Handles.color = Handles.zAxisColor;

		for ( int i = 0; i < component.path.Count; i++ )
		{
			// Handle position
			EditorGUI.BeginChangeCheck();
			Vector3 newPosition = Handles.PositionHandle( component.path[i], Quaternion.identity );

			if ( EditorGUI.EndChangeCheck() )
			{
				Undo.RecordObject( component, "change nav mesh path node" );

				component.path[i] = newPosition;
			}

			// Draw lines
			int nextIdx = (i + 1) % component.path.Count;
			Handles.ArrowHandleCap( 0,
				component.path[i],
				Quaternion.LookRotation( component.path[nextIdx] - component.path[i] ),
				Vector3.Distance( component.path[i], component.path[nextIdx] ),
				EventType.Repaint );
		}
	}
}
