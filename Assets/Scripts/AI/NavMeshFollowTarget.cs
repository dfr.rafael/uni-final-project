﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

[RequireComponent( typeof( NavMeshAgent ) )]
public class NavMeshFollowTarget : MonoBehaviour
{

	//
	// ATTRIBUTES
	//
	[SerializeField]
	private Transform target;

	[SerializeField]
	private float minDistanceToFollow = 10f;

	[SerializeField]
	private bool checkLineOfSight = false;

	[SerializeField, NaughtyAttributes.ShowIf( "checkLineOfSight" )]
	private LayerMask lineOfSightMask;

	[SerializeField, Tooltip( "Seconds between each path recalculation." )]
	private float pathUpdateSeconds = 0.25f;

	[SerializeField]
	private UnityEvent onStartFollowing;

	[SerializeField]
	private UnityEvent onStopFollowing;


	private Vector3 lastPosition = Vector3.zero;
	private bool canFollow = true;

	//
	// CACHED
	//
	private NavMeshAgent agent;
	private Coroutine updatePathCoroutine;
	private float minDistanceToFollowSqrd;
	private bool wasFollowing = false;

	private void Awake()
	{
		agent = GetComponent<NavMeshAgent>();
	}

	private void Start()
	{
		minDistanceToFollowSqrd = minDistanceToFollow * minDistanceToFollow;
	}

	private void FixedUpdate()
	{
		if ( !canFollow ) return;

		bool newFollowing = CheckIsFollowing( out float agentDistanceSqrd );

		// Was stopped and started moving
		if ( newFollowing && !wasFollowing )
		{

			if ( updatePathCoroutine != null )
				StopCoroutine( updatePathCoroutine );
			updatePathCoroutine = StartCoroutine( UpdatePath() );

			if ( onStartFollowing != null )
				onStartFollowing.Invoke();
		}

		// Was following and stopped
		if ( !newFollowing && wasFollowing )
		{

			agent.ResetPath();

			if ( updatePathCoroutine != null )
				StopCoroutine( updatePathCoroutine );

			if ( onStopFollowing != null )
				onStopFollowing.Invoke();
		}

		// Keep rotating agent towards target if inside stopping distance
		if ( newFollowing && agent.remainingDistance < 0.01f )
		{
			float newRotY = Quaternion.LookRotation( (target.position - transform.position).normalized, Vector3.up ).eulerAngles.y;
			transform.rotation = Quaternion.Slerp( transform.rotation, Quaternion.Euler( 0f, newRotY, 0f ), Time.deltaTime * agent.angularSpeed );
		}

		wasFollowing = newFollowing;
	}

	private bool CheckIsFollowing(out float agenteDistanceSqrd)
	{
		agenteDistanceSqrd = Vector3.SqrMagnitude( target.position - transform.position );

		// Agent only follows when target is inside min distance
		if ( agenteDistanceSqrd <= minDistanceToFollowSqrd )
		{
			// Check for clear line of sight
			if ( checkLineOfSight )
			{
				if ( Physics.Raycast( transform.position, (target.position - transform.position).normalized, out RaycastHit hit, minDistanceToFollow, lineOfSightMask.value ) )
				{
					// Check if collision is neither this nor target
					GameObject collidedObject = hit.collider.gameObject;
					return collidedObject == gameObject || collidedObject == target;
				}
				return true;
			}
			else
				return true;
		}

		return false;
	}

	/// <summary>
	/// Allows movement temporarily (until disallowed).
	/// </summary>
	public void AllowMovement()
	{
		canFollow = true;

		if ( updatePathCoroutine != null )
			StopCoroutine( updatePathCoroutine );
		updatePathCoroutine = StartCoroutine( UpdatePath() );
	}

	/// <summary>
	/// Disallows movement temporarily (until allowed).
	/// </summary>
	public void DisallowMovement()
	{
		canFollow = false;

		if ( updatePathCoroutine != null )
			StopCoroutine( updatePathCoroutine );
	}

	/// <summary>
	/// Stops movement altogether, with no possibility on starting again.
	/// </summary>
	public void Stop()
	{
		canFollow = false;

		if ( updatePathCoroutine != null )
			StopCoroutine( updatePathCoroutine );

		agent.enabled = false;
	}

	private IEnumerator UpdatePath()
	{
		while ( true )
		{
			if ( !agent.enabled ) break;

			Vector3 newPosition = target.position;

			if ( newPosition != lastPosition )
			{
				lastPosition = newPosition;
				agent.destination = newPosition;
			}

			yield return new WaitForSeconds( pathUpdateSeconds );

		}
	}

	public void SetTarget(Transform newTarget)
	{
		target = newTarget;
	}

	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;

		Gizmos.DrawWireSphere( transform.position, minDistanceToFollow );
	}
}
