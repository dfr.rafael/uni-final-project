﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using UnityEditor;
using UnityEngine;

public class SpawnerTimer : MonoBehaviour {

    [SerializeField]
    private int secondsPerSpawn = 10;

    [SerializeField]
    private int minEnemiesToSpawn = 1;

    [SerializeField]
    private int maxEnemiesToSpawn = 2;

    [SerializeField]
    private List<Transform> spawnPositions = new List<Transform>();

    [SerializeField]
    private Spawner spawner;

    [SerializeField]
    private BezierAttributes bezierAttributes;

    private bool running = true;

    private void OnEnable()
    {
        running = true;
        StartTimer();
    }

    private void OnDisable() => running = false;

    private void StartTimer()
    {
        StopCoroutine(SpawnTimer());
        StartCoroutine(SpawnTimer());
    }

    private IEnumerator SpawnTimer()
    {
        while (running)
        {
            yield return new WaitForSeconds(secondsPerSpawn);

            int enemiesToSpawn = Random.Range(minEnemiesToSpawn, maxEnemiesToSpawn);
            enemiesToSpawn.For(() =>
            {
                var spawnPosition = spawnPositions[Random.Range(0, spawnPositions.Count)];
                spawner.Spawn(spawnPosition.position);
            });
        }
    }

    public void FlipEnabled()
    {
        enabled = !enabled;
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Vector3 offset = new Vector3(0f, bezierAttributes.linePower, 0f);

        Handles.zTest = UnityEngine.Rendering.CompareFunction.LessEqual;

        Handles.color = bezierAttributes.lineColor;
        foreach (var currentTransform in spawnPositions)
        {
            Vector3 position = currentTransform.position;

            Handles.DrawBezier(transform.position, position,
                transform.position - offset, position + offset,
                bezierAttributes.lineColor, new Texture2D(2, 2), bezierAttributes.lineWidth);

            Handles.DrawSolidDisc(position, Vector3.down, 1f);
        }
    }
#endif
}
