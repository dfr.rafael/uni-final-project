﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

	[SerializeField]
	protected bool useOffset;
	[SerializeField, NaughtyAttributes.ShowIf( "useOffset" )]
	protected Vector3 offset;

	public virtual void Spawn(Vector3 position) { throw new System.Exception( "Method not implemented." ); }

	public virtual void SpawnOffset() { throw new System.Exception( "Method not implemented." ); }

	public virtual void Spawn(Vector3 position, GameObject customPrefab)
	{
		Instantiate( customPrefab, position, Quaternion.identity );
	}
}
