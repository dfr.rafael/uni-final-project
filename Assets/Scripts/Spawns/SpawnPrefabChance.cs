﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPrefabChance : SpawnPrefab {

    [SerializeField, Range(0f, 1f)]
    protected float chance;

    public override void Spawn(Vector3 position)
    {
        if (Random.Range(0f, 1f) >= chance) base.Spawn(position);
    }
}