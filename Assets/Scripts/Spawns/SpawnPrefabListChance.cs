﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PrefabAndChance
{
	public GameObject prefab;
	public int chance;
	[HideInInspector]
	public float normalizedChance;

	public void SetNormalizedChance(float chance) => normalizedChance = chance;
}

public class SpawnPrefabListChance : Spawner
{

	[SerializeField]
	private List<PrefabAndChance> prefabs = new List<PrefabAndChance>();

	private void Awake()
	{
		int totalChance = 0;
		prefabs.ForEach( el => totalChance += el.chance );
		prefabs.ForEach( el => el.normalizedChance = el.chance / (float)totalChance );
	}

	public override void Spawn(Vector3 position)
	{
		float randomNumber = Random.Range( 0f, 1f );
		float summedChances = 0f;
		foreach ( var prefabAndChance in prefabs )
		{
			summedChances += prefabAndChance.normalizedChance;
			if ( summedChances >= randomNumber )
			{
				base.Spawn( position, prefabAndChance.prefab );
				break;
			}
		}
	}

	public override void SpawnOffset()
	{
		float randomNumber = Random.Range( 0f, 1f );
		float summedChances = 0f;
		foreach ( var prefabAndChance in prefabs )
		{
			summedChances += prefabAndChance.normalizedChance;
			if ( summedChances >= randomNumber )
			{
				base.Spawn( transform.position + transform.TransformDirection( offset ), prefabAndChance.prefab );
				break;
			}
		}
	}

	private void OnDrawGizmosSelected()
	{
		if ( !useOffset ) return;
		Gizmos.color = Color.red;
		Gizmos.DrawLine( transform.position, transform.position + transform.TransformDirection( offset ) );
		Gizmos.DrawWireCube( transform.position + transform.TransformDirection( offset ), new Vector3( 0.1f, 0.1f, 0.1f ) );
	}
}
