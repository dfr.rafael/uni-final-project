﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPrefab : Spawner
{

	[SerializeField]
	protected GameObject prefab;

	public override void SpawnOffset()
	{
		Spawn( transform.position + transform.TransformDirection( offset ) );
	}

	public override void Spawn(Vector3 position)
	{
		Instantiate( prefab, position, Quaternion.identity );
	}

	private void OnDrawGizmosSelected()
	{
		if ( !useOffset ) return;
		Gizmos.color = Color.red;
		Gizmos.DrawLine( transform.position, transform.position + transform.TransformDirection(offset) );
		Gizmos.DrawWireCube( transform.position + transform.TransformDirection( offset ), new Vector3(0.1f, 0.1f, 0.1f) );
	}
}
