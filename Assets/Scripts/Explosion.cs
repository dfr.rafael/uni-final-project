﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{

    [SerializeField]
    private float explosionRadius;

    [SerializeField]
    private float explosionForce;

    [SerializeField]
    private LayerMask explosionMask;

    public void Explode()
    {
        var colliders = Physics.OverlapSphere(transform.position, explosionRadius, explosionMask);

        foreach(var collider in colliders)
        {
            var rb = collider.GetComponent<Rigidbody>();
            rb?.AddExplosionForce(explosionForce, transform.position, explosionRadius);
        }

        Destroy(gameObject);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }

}
