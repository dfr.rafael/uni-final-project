using Cinemachine;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent( typeof( CinemachineFreeLook ) )]
public class FreelookInputSystem : MonoBehaviour
{

    private CinemachineFreeLook freeLook;

    public void Start()
    {
        freeLook = GetComponent<CinemachineFreeLook>();
    }

    public void OnLook(InputAction.CallbackContext context)
    {
        // Normalize the vector to have an uniform vector in whichever form it came from (I.E Gamepad, mouse, etc)
        Vector2 lookMovement = context.ReadValue<Vector2>().normalized;

        // This is because X axis is only contains between -180 and 180 instead of 0 and 1 like the Y axis
        lookMovement.x = lookMovement.x * 180f;

        // Adjust axis values using look speed and Time.deltaTime so the look doesn't go faster if there is more FPS
        freeLook.m_XAxis.Value += lookMovement.x * freeLook.m_XAxis.m_AccelTime * Time.deltaTime * (freeLook.m_XAxis.m_InvertInput ? 1 : -1);
        freeLook.m_YAxis.Value += lookMovement.y * freeLook.m_XAxis.m_AccelTime * Time.deltaTime * (freeLook.m_YAxis.m_InvertInput ? 1 : -1);

        freeLook.m_XAxis.Value = Mathf.Clamp( freeLook.m_XAxis.Value, freeLook.m_XAxis.m_MinValue, freeLook.m_XAxis.m_MaxValue);
    }
}
