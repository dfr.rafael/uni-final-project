﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookToMouse : MonoBehaviour
{

    private new Camera camera;

    private void Awake()
    {
        camera = Camera.main;
    }

    void Update()
    {
        Vector3 mousePosition = Input.mousePosition;
        Ray screenRay = camera.ScreenPointToRay(mousePosition);

        Vector3 mouseWorldPosition;

        // Raycast against the ground
        if (Physics.Raycast(screenRay, out RaycastHit hitInfo))
        {
            mouseWorldPosition = hitInfo.point;
        }
        else
        {
            return;
        }

        Vector3 targetPosition = mouseWorldPosition;
        targetPosition.y = transform.position.y;

        transform.LookAt(targetPosition);

        //float rotationAngle = Vector2.SignedAngle(Vector2.up, new Vector2(-targetPosition.x, targetPosition.z));;
        //transform.rotation = Quaternion.Euler(0f, rotationAngle, 0f
    }
}
