using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTargetToMouse : MonoBehaviour
{
    [SerializeField]
    private bool keepY;
    private float startY;

    private new Camera camera;

	private void Start()
	{
        camera = Camera.main;
        startY = transform.position.y;
	}

	private void Update()
	{
        if (Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out RaycastHit hitInfo))
        {
            transform.position = new Vector3(hitInfo.point.x, !keepY ? hitInfo.point.y : startY, hitInfo.point.z);
        }
    }

}
