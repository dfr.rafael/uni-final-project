using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTargetToMousePlane : MonoBehaviour
{
    [SerializeField]
    private bool keepY;
    private float startY;

    [SerializeField]
    private Vector3 planeNormal = Vector3.up;

    [SerializeField]
    private Transform planePivot;

    private new Camera camera;
    private Plane plane;

    private void Start()
    {
        camera = Camera.main;
        plane = new Plane(planeNormal, planePivot.position);
        startY = transform.position.y;
    }

    private void Update()
    {
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        if (plane.Raycast(ray, out float hitDistance))
        {
            Vector3 hitPos = ray.GetPoint(hitDistance);
            transform.position = new Vector3(hitPos.x, !keepY ? hitPos.y : startY, hitPos.z);

            //Vector3 rayDirIgnoreY = ray.direction;
            //rayDirIgnoreY.y = 0;
            //rayDirIgnoreY.Normalize();
            // Not really working, fix it!
            //float angleRayToGround = Vector3.Angle(ray.direction, rayDirIgnoreY);
            //float angleSin = Mathf.Cos(angleRayToGround);
            //Vector3 hitPos = ray.GetPoint(hitDistance - (1.0f / angleSin));
            //transform.position = hitPos;
        }
    }
}
