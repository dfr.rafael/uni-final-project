using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookToMouseContraints : MonoBehaviour {

	[SerializeField]
	private bool useX;

	[SerializeField]
	private bool useY;

	[SerializeField]
	private bool useZ;

	private new Camera camera;
	private float x, y, z;

	private void Awake()
	{
		camera = Camera.main;
		x = useX ? 1f : 0f;
		y = useY ? 1f : 0f;
		z = useZ ? 1f : 0f;
	}

	void Update()
	{
		Vector3 mousePosition = Input.mousePosition;
		Ray screenRay = camera.ScreenPointToRay(mousePosition);

		Vector3 mouseWorldPosition;

		// Raycast against the ground
		if (Physics.Raycast(screenRay, out RaycastHit hitInfo))
		{
			mouseWorldPosition = hitInfo.point;
		}
		else
		{
			return;
		}

		Vector3 targetPosition = mouseWorldPosition;
		targetPosition.y = transform.position.y;

		Quaternion lookQuat = Quaternion.LookRotation(targetPosition - transform.position);
		Vector3 dir = lookQuat.eulerAngles;

		transform.eulerAngles = new Vector3(dir.x * x, dir.y * y, dir.z * z);
	}
}
