using UnityEngine;
using UnityEngine.InputSystem;

public class LookToMousePlane : MonoBehaviour
{

    [SerializeField]
    private Vector3 planeNormal = Vector3.up;

	[SerializeField]
	private Transform planePivot;
	

    private new Camera camera;
	private Plane plane;

	private void Awake()
	{
		camera = Camera.main;
		plane = new Plane(planeNormal, planePivot.position);
	}

	void Update()
	{
		Vector3 mousePosition = Mouse.current.position.ReadValue();
		Ray screenRay = camera.ScreenPointToRay(mousePosition);

		Vector3 mouseWorldPosition;

		// Raycast against the ground plane
		if (plane.Raycast(screenRay, out float hitOut))
		{
			mouseWorldPosition = screenRay.GetPoint(hitOut);
		}
		else
		{
			return;
		}

		Vector3 targetPosition = mouseWorldPosition;

		Quaternion lookQuat = Quaternion.LookRotation(targetPosition - transform.position);
		Vector3 dir = lookQuat.eulerAngles;

		transform.eulerAngles = new Vector3(0.0f, dir.y, 0.0f);
	}
}
