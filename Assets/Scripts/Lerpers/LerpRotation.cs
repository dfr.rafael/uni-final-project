﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LerpRotation : MonoBehaviour
{

	[SerializeField]
	private Vector3 offset;

	[SerializeField]
	private float duration = 1f;

	[SerializeField]
	private AnimationCurve lerpCurve;

	[SerializeField]
	private bool executeOnStart = false;

	[SerializeField]
	private bool loop = false;

	[SerializeField]
	private UnityEvent onEndLerp;

	private Quaternion startRotation;

	private void Start()
	{
		if ( executeOnStart ) Activate();
	}

	public void Activate()
	{
		startRotation = transform.rotation;
		StartCoroutine( RotationTimer() );
	}

	private IEnumerator RotationTimer()
	{
		Quaternion finalRotation = Quaternion.Euler( startRotation.eulerAngles + offset );

		float t = 0.0f;
		while ( t < duration )
		{
			t += Time.deltaTime;
			float normalizedT = t / duration;

			transform.rotation = Quaternion.Lerp( startRotation, finalRotation, lerpCurve.Evaluate( normalizedT ) );

			yield return new WaitForEndOfFrame();
		}

		if ( loop )
			Activate();

		if ( onEndLerp != null )
			onEndLerp.Invoke();
	}

}
