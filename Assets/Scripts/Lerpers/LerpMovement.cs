﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LerpMovement : MonoBehaviour
{

	[SerializeField]
	private Vector3 offset;

	[SerializeField]
	private float duration = 1f;

	[SerializeField]
	private AnimationCurve lerpCurve;

	[SerializeField]
	private bool executeOnStart = false;

	[SerializeField]
	private bool loop = false;

	[SerializeField]
	private UnityEvent onEndLerp;

	private Vector3 startPosition;

	private void Start()
	{
		if ( executeOnStart ) Activate();
	}

	public void Activate()
	{
		startPosition = transform.position;
		StartCoroutine( MoveTimer() );
	}

	private IEnumerator MoveTimer()
	{
		Vector3 finalPosition = startPosition + offset;

		float t = 0.0f;
		while ( t < duration )
		{
			t += Time.deltaTime;
			float normalizedT = t / duration;

			transform.position = Vector3.Lerp( startPosition, finalPosition, lerpCurve.Evaluate( normalizedT ) );

			yield return new WaitForEndOfFrame();
		}

		if ( loop )
			Activate();

		if ( onEndLerp != null )
			onEndLerp.Invoke();
	}

	private void OnDrawGizmos()
	{
		Gizmos.color = new Color( 126 / 256f, 67 / 256f, 230 / 256f );

		Gizmos.DrawLine( transform.position, transform.position + offset );
		Gizmos.DrawWireSphere( transform.position + offset, 0.2f );
	}

}
