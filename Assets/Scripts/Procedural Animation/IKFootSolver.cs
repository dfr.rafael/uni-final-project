using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKFootSolver : MonoBehaviour
{

	[Header( "Foot Config" )]
	[SerializeField] private Vector3 footOffset;

	[Header("References")]
	[SerializeField] private Transform body;
	[SerializeField] private IKFootSolver otherFoot;

	[Header( "Step" )]
	[SerializeField] private float stepDistance = 1f;
	[SerializeField] private float stepLength = 1f;
	[SerializeField] private float stepHeight = 1f;
	[SerializeField] private float stepSpeed = 1f;

	[Header("Parameters")]
	[SerializeField] private LayerMask layer;

	private float footSpacing;
	private Vector3 footPosition, currentPosition, oldPosition;

	private float stepLerp = 1f;

#if UNITY_EDITOR
	[Header( "Gizmos" )]
	[SerializeField] private float radius = 1f;
#endif

	private void Start()
	{
		footSpacing = transform.localPosition.x;

		footPosition = currentPosition = oldPosition = transform.position;

		stepLerp = 1f;
	}

	void Update()
	{
		Position();
	}

	private bool GetHitPosition(out Vector3 position)
	{
		Ray ray = new Ray( body.position + (body.right * footSpacing), Vector3.down );
		if ( Physics.Raycast( ray, out RaycastHit hit, 100, layer.value ) )
		{
			position = hit.point;
			return true;
		}
		position = Vector3.zero;
		return false;
	}

	[NaughtyAttributes.Button]
	private void Position()
	{
		transform.position = footPosition;

		if ( GetHitPosition( out Vector3 hitPosition ) )
		{
			if ( Vector3.Distance( currentPosition, hitPosition ) > stepDistance && stepLerp >= 1 && !otherFoot.IsMoving())
			{
				stepLerp = 0;

				int direction = body.InverseTransformPoint( hitPosition ).z > body.InverseTransformPoint( currentPosition ).z ? 1 : -1;
				currentPosition = hitPosition + (body.forward * stepLength * direction) + footOffset;
			}
		}

		if ( stepLerp < 1f )
		{
			footPosition = Vector3.Lerp( oldPosition, currentPosition, stepLerp );
			footPosition.y += Mathf.Sin( stepLerp * Mathf.PI ) * stepHeight;

			stepLerp += Time.deltaTime * stepSpeed;

			if(stepLerp > 1f )
			{
				footPosition = Vector3.Lerp( oldPosition, currentPosition, 1.0f );
				footPosition.y += Mathf.Sin( 1.0f * Mathf.PI ) * stepHeight;
			}
		}
		else
		{
			oldPosition = currentPosition;
		}

	}

#if UNITY_EDITOR
	private void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawSphere( currentPosition, radius );

		//if ( GetHitPosition( out Vector3 hitPosition ) )
		//{
		//	Gizmos.color = Color.green;
		//	Gizmos.DrawSphere( hitPosition, radius );
		//}
	}
#endif

	public bool IsMoving()
	{
		return stepLerp < 1f;
	}
}
