﻿using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using UnityEngine;

[ExecuteAlways, RequireComponent(typeof(Health))]
public class Enemy : MonoBehaviour {

    static private EnemiesManager enemiesManager;

    private Health health;

    public Health Health { get; }

    private void OnEnable() => AddToList();
    private void OnDisable() => RemoveFromList();

    private void Start()
    {
        health = GetComponent<Health>();
    }

    private void AddToList()
    {
        if (enemiesManager == null) enemiesManager = FindObjectOfType<EnemiesManager>();
        enemiesManager?.Register(this);
    }

    private void RemoveFromList()
    {
        if (enemiesManager == null) enemiesManager = FindObjectOfType<EnemiesManager>();
        enemiesManager?.Unregister(this);
    }
}
