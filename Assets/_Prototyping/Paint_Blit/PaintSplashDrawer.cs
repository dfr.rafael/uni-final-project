﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

public class PaintSplashDrawer : MonoBehaviour {

    [SerializeField]
    private Texture splashTexture;

    private new Camera camera;

    void Awake()
    {
        camera = Camera.main;
    }

    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            CastPaintSplash(new Vector4(1, 0, 0, 0));
        }
        else if (Input.GetMouseButton(1))
        {
            CastPaintSplash(new Vector4(0, 1, 1, 0));
        }
    }

    private void CastPaintSplash(Vector4 mask)
    {
        Ray cameraRay = camera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(cameraRay, out RaycastHit hitInfo))
        {
            hitInfo.collider.GetComponent<PaintSplash>()?.BlitSplash(splashTexture, hitInfo.textureCoord, mask);
        }
    }
}
