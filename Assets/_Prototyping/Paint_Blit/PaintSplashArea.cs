﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintSplashArea : MonoBehaviour
{
    [SerializeField]
    private Texture splashTexture;

    [SerializeField]
    private float area;

    [SerializeField]
    private Vector4 mask;

    private float Random01() => Random.Range(0f, 1f);

    public void CastPaintSplash(Collision collision)
    {
        Ray cameraRay = new Ray(transform.position, (collision.GetContact(0).point - transform.position).normalized);
        Debug.Log(cameraRay.ToString());
        if (Physics.Raycast(cameraRay, out RaycastHit hitInfo))
        {
            hitInfo.collider.GetComponent<PaintSplash>()?.BlitSplash(splashTexture, hitInfo.textureCoord, mask);
        }
    }
}
