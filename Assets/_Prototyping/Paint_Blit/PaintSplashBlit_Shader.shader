﻿Shader "Paint/Blit"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Scale ("Splash Scale", float) = 1.0
    }

    CGINCLUDE
        #include "UnityCG.cginc"

        sampler2D _MainTex;
        float4 _MainTex_ST;
        sampler2D _SplashTexture;
        float4x4 _WorldMatrix;
        float4 _Coordinate;
        half4 _Mask;
        float _Angle;
        float _Scale;

        fixed4 _ColorA;
        fixed4 _ColorB;

        struct VertexData {
            float4 uv : TEXCOORD0;
            float4 vertex : POSITION;
        };

        struct Interpolators {
            float2 uv : TEXCOORD0;
            float4 vertex : SV_POSITION;
            //float3 worldPos : TEXCOORD1;
        };

        Interpolators VertexProgram (VertexData v){
            Interpolators i;
            i.vertex = UnityObjectToClipPos(v.vertex);
            i.uv = TRANSFORM_TEX(v.uv, _MainTex);

            // Convert 0...1 UV space to -1...1 clip space.
            // i.worldPos = float3(v.uv.x * 2.0f - 1.0f, v.uv.y * 2.0f - 1.0f, 1);

            return i;
        }
    ENDCG

    SubShader
    {
        ZTest Off
		Cull Off
		ZWrite Off
		Fog { Mode off }

        Pass // 0
        {
            Name "Splat"
            CGPROGRAM
            #pragma vertex VertexProgram
            #pragma fragment FragmentProgram
            #pragma target 3.0

            float2 rotateUV(float2 uv, float rotation, float2 mid)
            {
                return float2(
                    cos(rotation) * (uv.x - mid.x) + sin(rotation) * (uv.y - mid.y) + mid.x,
                    cos(rotation) * (uv.y - mid.y) - sin(rotation) * (uv.x - mid.x) + mid.y
                );
            }

            fixed4 FragmentProgram (Interpolators i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);

                float2 uv = i.uv;
                uv *= _Scale;
                uv += 0.5 - _Coordinate.xy * _Scale;
                uv = rotateUV(uv, _Angle, float2(0.5,0.5));

                float draw = tex2D(_SplashTexture, uv).r;

                col = min( col, 1.0 - draw * ( 1.0 - _Mask ) );
				col = max( col, draw * _Mask);

                return saturate(col);
            }
            ENDCG
        }
    }
}
