﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class PaintSplash : MonoBehaviour {

    private RenderTexture currentTexture;

    [SerializeField]
    private int width = 1024, height = 1024;

    [NonSerialized]
    private Material blitMaterial;

    private void Awake()
    {
        currentTexture = new RenderTexture(width, height, 0, RenderTextureFormat.ARGBFloat);
        currentTexture.Create();
        GetComponent<MeshRenderer>().material.SetTexture("_MainTexture", currentTexture);

        blitMaterial = new Material(Shader.Find("Paint/Blit"));
        blitMaterial.hideFlags = HideFlags.HideAndDontSave;
    }

    public void BlitSplash(Texture splashTexture, Vector2 hitPoint, Vector4 mask)
    {
        blitMaterial.SetVector("_Coordinate", new Vector4(hitPoint.x, hitPoint.y, 0, 0));
        blitMaterial.SetMatrix("_WorldMatrix", transform.localToWorldMatrix);
        blitMaterial.SetVector("_Mask", mask);
        blitMaterial.SetFloat("_Angle", Random.Range(0f, 360f));
        blitMaterial.SetFloat("_Scale", 5f);

        RenderTexture temp = RenderTexture.GetTemporary(currentTexture.width, currentTexture.height, 0, RenderTextureFormat.ARGBFloat);
        temp.Create();

        Graphics.Blit(currentTexture, temp);
        blitMaterial.SetTexture("_SplashTexture", splashTexture);

        Graphics.Blit(temp, currentTexture, blitMaterial, 0);

        RenderTexture.ReleaseTemporary(temp);
    }

    private void OnGUI()
    {
        //GUI.DrawTexture(new Rect(0,0,256,256), currentTexture, UnityEngine.ScaleMode.ScaleToFit, false, 1);
    }

}
