using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintVertexParticleCollision : MonoBehaviour
{
	//[SerializeField]
	//private PaintVertexColorEmitter colorEmitter;

	[SerializeField]
	private Painter painter;

	private ParticleSystem part;
	private List<ParticleCollisionEvent> collisionEvents;

	private void Awake()
	{
		part = GetComponent<ParticleSystem>();
		collisionEvents = new List<ParticleCollisionEvent>();
	}

	void OnParticleCollision(GameObject other)
	{
		int numCollisionEvents = part.GetCollisionEvents( other, collisionEvents );

		int i = 0;

		while ( i < numCollisionEvents )
		{
			painter.Activate( collisionEvents[i].intersection );
			i++;
		}
	}
}
