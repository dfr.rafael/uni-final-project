using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PaintVertexCollision : MonoBehaviour
{
    [SerializeField]
    private PaintVertexColorEmitter colorEmitter;

	private void Awake()
	{
		if (!colorEmitter)
		{
			colorEmitter = GetComponent<PaintVertexColorEmitter>();
		}
	}

	private void OnCollisionEnter(Collision collision)
	{
		colorEmitter.EmitColor(collision.GetContact(0).point);
	}
}
