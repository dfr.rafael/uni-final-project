﻿using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintVertexColorEmitter : MonoBehaviour
{

    [SerializeField]
    private float radius;

	[SerializeField]
	private Vector4 mask;

	[SerializeField]
	private bool useRandom;

	[SerializeField, ShowIf("useRandom")]
	private Vector2 randomRange;

	private void Start()
	{
		if (useRandom)
		{
			mask.x = Random.Range(randomRange.x, randomRange.y);
			mask.y = Random.Range(randomRange.x, randomRange.y);
			mask.z = Random.Range(randomRange.x, randomRange.y);
			mask.w = Random.Range(randomRange.x, randomRange.y);
		}

	}

	public void EmitColor(Vector3 collisionPoint)
	{
		Collider[] colliders = Physics.OverlapSphere(transform.position, radius);

		foreach (Collider collider in colliders)
		{
			PaintVertexColorDistance paintable = collider.GetComponent<PaintVertexColorDistance>();
			paintable?.UpdateDistance(collisionPoint, radius, mask);
		}
	}

	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.green;
		Gizmos.DrawWireSphere(transform.position, radius);
	}

}
