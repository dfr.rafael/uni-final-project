﻿using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Security.Policy;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UIElements;

public class PaintVertexColorDistance : MonoBehaviour
{

	[SerializeField]
	private UnityEvent onUpdateDistance;
	[SerializeField]
	private UnityEvent<float> onUpdateDistanceFloat;

	[SerializeField]
	private bool canUpdate = true;

	[SerializeField]
	private bool lockUpdateOnMax = false;

	[SerializeField]
	private List<NumberModifier> numberModifiers = new List<NumberModifier>();

	private MeshFilter meshFilter;

	[SerializeField, BoxGroup( "Debug" )]
	private float debugRadius;
	[SerializeField, BoxGroup( "Debug" )]
	private Vector3 debugPivotWorldPos;
	[SerializeField, BoxGroup( "Debug" )]
	private Vector4 debugMask;
	[SerializeField, BoxGroup( "Debug" )]
	private bool debugIsAdditive;

	private const float linearFalloff = 10f;
	private const float quadraticFalloff = 2f;

	private MeshFilter MeshFilter
	{
		get
		{
#if UNITY_EDITOR
			if ( meshFilter == null )
			{
				meshFilter = GetComponent<MeshFilter>();
			}
#endif
			return meshFilter;
		}
	}

	private Color[] MeshColors
	{
		get
		{
#if UNITY_EDITOR
			return Application.isPlaying
				? MeshFilter.mesh.colors
				: MeshFilter.sharedMesh.colors;
#else
		return MeshFilter.mesh.colors;
#endif
		}

		set
		{
#if UNITY_EDITOR
			if ( Application.isPlaying )
				MeshFilter.mesh.colors = value;
			else
				MeshFilter.sharedMesh.colors = value;
#else
		MeshFilter.mesh.colors = value;
#endif
		}
	}

	private Vector3[] meshVerticesCache = new Vector3[0];
	private Vector3[] MeshVertices
	{
		get
		{
			if ( meshVerticesCache.Length == 0 )
			{
#if UNITY_EDITOR
				meshVerticesCache = Application.isPlaying
					? MeshFilter.mesh.vertices
					: MeshFilter.sharedMesh.vertices;
#else
				meshVerticesCache = MeshFilter.mesh.vertices;
#endif
			}
			return meshVerticesCache;
		}
	}

	void Start()
	{
		meshFilter = GetComponent<MeshFilter>();

		ResetVertexColors();

		// Set lock event
		if ( lockUpdateOnMax )
		{
			onUpdateDistanceFloat.AddListener( (value) =>
			 {
				 if ( value == 1.0f )
				 {
					 canUpdate = false;
				 }
			 } );
		}
	}

	public void LockUpdates()
	{
		canUpdate = false;
	}

	public void AddNumberModifier(NumberModifier numberModifier)
	{
		numberModifiers.Add( numberModifier );
	}

	[Button( "Update Distance" )]
	private void UpdateDistaceDebug()
	{
		UpdateDistance( debugPivotWorldPos, debugRadius, debugMask, debugIsAdditive );
	}

	private void ResetVertexColors()
	{
		Color[] colors = new Color[MeshVertices.Length];

		for ( int i = 0; i < colors.Length; i++ )
		{
			colors[i] = Color.black;
		}

		MeshColors = colors;
	}

	public void UpdateDistance(Vector3 pivotWorldPosition, float radius, Vector4 mask, bool additive = true)
	{
		if ( !canUpdate ) return;

		foreach ( NumberModifier numberModifier in numberModifiers )
		{
			mask.x = numberModifier.Modify( mask.x );
			mask.y = numberModifier.Modify( mask.y );
			mask.z = numberModifier.Modify( mask.z );
			mask.w = numberModifier.Modify( mask.w );
		}

		Vector3[] vertices = MeshVertices;

		Color[] colors = MeshColors;
		if ( colors.Length == 0 )
		{
			colors = new Color[vertices.Length];
		}

		for ( int i = 0; i < vertices.Length; i++ )
		{
			Vector3 worldVertice = transform.localToWorldMatrix.MultiplyPoint( vertices[i] );
			float distance = Vector3.Distance( worldVertice, pivotWorldPosition );

			// [0,1]
			float normalizedDistance = distance / radius;

			// Inverting normalized distance to have 1 in the center and 0 in the borders
			normalizedDistance = 1f - normalizedDistance;

			normalizedDistance = normalizedDistance * linearFalloff + Mathf.Pow( normalizedDistance, 2 ) * quadraticFalloff;

			// x < 0 when inside circle
			// x == 0 when on circle border
			// x > 0 when outside circle
			float sdf = distance - radius;

			// Neg | 0 | Pos
			//  -1 | 0 | 1
			float sign = sdf.Sign();

			// [-1, +1] -> [0, 1] (inverted)
			float color = ((sign * -1f) + 1) / 2f;

			Color newColor = new Color( color * mask.x, color * mask.y, color * mask.z, color * mask.w );
			newColor *= normalizedDistance;

			if ( additive )
			{
				colors[i] = (colors[i] + newColor).Clamp01();
			}
			else
			{
				colors[i] = newColor.Clamp01();
			}
		}

		MeshColors = colors;

		if ( onUpdateDistance != null ) onUpdateDistance.Invoke();
		if ( onUpdateDistanceFloat != null ) onUpdateDistanceFloat.Invoke( ComputeCoveragePercentage() );
	}

	public void UpdateColorNormalized(float value)
	{
		int verticesLength = MeshVertices.Length;

		Color[] colors = MeshColors;
		if ( colors.Length == 0 )
		{
			colors = new Color[verticesLength];
		}

		for ( int i = 0; i < verticesLength; i++ )
		{

			Color newColor = new Color( value, value, value, 1.0f );
			colors[i] = newColor.Clamp01();
		}

		MeshColors = colors;

		if ( onUpdateDistance != null ) onUpdateDistance.Invoke();
		if ( onUpdateDistanceFloat != null ) onUpdateDistanceFloat.Invoke( ComputeCoveragePercentage() );
	}

	[Button( "Compute Coverage Percentage" )]
	public float ComputeCoveragePercentage()
	{
		Color[] colors = MeshColors;

		float sum = 0;
		foreach ( Color color in colors )
		{
			sum += (color.r + color.g + color.b) / 3f;
		}
		float percentage = sum / colors.Length;

		return percentage;
	}

#if UNITY_EDITOR
	private void OnDrawGizmosSelected()
	{
		Gizmos.color = new Color(1.0f, 0.0f, 1.0f, 1.0f);
		Gizmos.DrawWireSphere( debugPivotWorldPos, debugRadius );
	}
#endif

}
