using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[CreateAssetMenu( menuName = "Weapons/Hitscan", fileName = "Weapon Hitscan" )]
public class WeaponHitscan : IWeapon
{

	[Header( "Weapon Hitscan Config" )]
	[SerializeField] private GameObject prefab;
	[SerializeField] private float lineSeconds = 1.0f;
	[SerializeField] private float maxDistance = 1.0f;
	[SerializeField] private LayerMask layerMask;

	[Header( "Line Config" )]
	[SerializeField] private bool alwaysShow = false;
	[SerializeField] private bool lineFollowPivot = false;
	[SerializeField] private float lineWidth = 0.1f;
	[SerializeField] private Material lineMaterial;
	[SerializeField, ColorUsage( true, true )] private Color lineColorHit;
	[SerializeField, ColorUsage( true, true )] private Color lineColorMiss;
	[SerializeField] private AnimationCurve lineAlphaCurve = AnimationCurve.Linear( 0.0f, 1.0f, 1.0f, 0.0f );

	private LineRenderer lineRenderer;
	private Color currentColor;
	private float lineLerpTimer = 0.0f;

	private Vector3 LineStartPosition
	{
		get
		{
			return pivot.position - pivot.up;
		}
	}

	private bool IsLerping
	{
		get
		{
			return lineLerpTimer < lineSeconds;
		}
	}

	private void Attack()
	{
		lineRenderer.SetPosition( 0, LineStartPosition );

		if ( GetHitscan( out RaycastHit hit ) )
		{
			if ( !CheckCanAttack() ) return;
			currentColor = lineColorHit;

			lineRenderer.SetPosition( 1, hit.point );
			GameObject go = Instantiate( prefab, hit.point, Quaternion.identity );
			go.GetComponent<Painter>()?.Activate( true );
			go.GetComponent<HealthChangerArea>()?.ApplyHealthChange( hit.collider.gameObject );
			Destroy( go );

			lineLerpTimer = 0.0f; // Reset lerp timer
			ActivateCooldown();
		}
		else
		{
			currentColor = lineColorMiss;
			lineRenderer.SetPosition( 1, pivot.position + pivot.forward * maxDistance );
			lineLerpTimer = 0.0f; // Reset lerp timer
		}
	}

	private bool GetHitscan(out RaycastHit raycastHit)
	{
		Ray ray = new Ray( pivot.position, pivot.forward );
		if ( Physics.Raycast( ray, out RaycastHit hit, maxDistance, layerMask.value ) )
		{
			raycastHit = hit;
			return true;
		}
		raycastHit = default;
		return false;
	}

	public override void Start(GameObject controllerObject, Transform pivot)
	{
		base.Start( controllerObject, pivot );

		lineRenderer = pivot.gameObject.AddComponent<LineRenderer>();
		lineRenderer.startWidth = lineRenderer.endWidth = lineWidth;
		lineRenderer.startColor = lineRenderer.endColor = currentColor = lineColorMiss;
		if ( lineMaterial != null )
			lineRenderer.material = lineMaterial;
		else
			lineRenderer.material = new Material( Shader.Find( "Sprites/Default" ) );
		if ( !alwaysShow )
			lineRenderer.enabled = false;

		lineLerpTimer = lineSeconds; // Reset lerp timer
	}

	public override void Update()
	{
		base.Update();

		if ( alwaysShow )
		{
			if ( GetHitscan( out RaycastHit hit ) )
			{

				currentColor = lineColorHit;
				lineRenderer.SetPosition( 1, hit.point );
			}
			else
			{
				currentColor = lineColorMiss;
				lineRenderer.SetPosition( 1, pivot.position + pivot.forward * maxDistance );
			}
		}

		if ( lineFollowPivot )
			lineRenderer.SetPosition( 0, LineStartPosition );

		if ( lineLerpTimer < lineSeconds )
		{
			if ( !alwaysShow )
				lineRenderer.enabled = true;

			lineLerpTimer += Time.deltaTime;

			if ( !alwaysShow && lineLerpTimer >= lineSeconds )
			{
				lineRenderer.enabled = false;
				return;
			}

			float alpha = lineAlphaCurve.Evaluate( Mathf.Clamp01( lineLerpTimer / lineSeconds ) );
			lineRenderer.startColor = lineRenderer.endColor = new Color( currentColor.r, currentColor.g, currentColor.b, alpha );
		}
		else
		{
			lineRenderer.startColor = lineRenderer.endColor = currentColor;
		}
	}

	public override void Destroy()
	{
		Destroy( lineRenderer );
	}

	public override void OnAttackDown()
	{
		if ( attackOnDown )
			Attack();
	}

	public override void OnAttackHold()
	{
		if ( attackOnHold )
			Attack();
	}

	public override void OnAttackUp()
	{
		if ( attackOnUp )
			Attack();
	}
}
