using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

[CreateAssetMenu( menuName = "Weapons/Projectile", fileName = "Weapon Projectile" )]
public class WeaponProjectile : IWeapon
{

	[Header( "Weapon Projectile Config" )]
	[SerializeField, Required]
	private GameObject projectilePrefab;
	[SerializeField] private bool aimScreenCenter = false;

	[Header( "Chargeable" )]
	[SerializeField]
	private bool chargeOnHold = true;
	[SerializeField, ShowIf( "chargeOnHold" ), Tooltip( "Seconds to reach the max charge." )]
	private float chargeTime = 1.0f;
	[SerializeField, ShowIf( "chargeOnHold" )]
	private WeaponChargeable chargeable;
	private float charge = 0.0f;
	private float normalizedCharge = 0.0f;

	[Header( "Carry Velocity" )]
	[SerializeField] private bool carryVelocity = false;
	[SerializeField, ShowIf( "carryVelocity" ), Range( 0.0f, 1.0f )]
	private float carryVelocityRatio = 0.5f;
	[SerializeField, ShowIf( "carryVelocity" ), Tooltip( "Max velocity the carrier can reach." )]
	private float maxCarrierVelocity = 78f;
	[SerializeField, ShowIf( "carryVelocity" ), Tooltip( "Velocity multiplier from zero to Max Carrier Velocity" )]
	private AnimationCurve carryVelocityCurve = AnimationCurve.Linear( 0.0f, 0.0f, 1.0f, 1.0f );
	private const float minCarryVelocity = 0.01f;

	[Header( "Multiple Bullets" )]
	[SerializeField] private int attackNumber = 1;
	[SerializeField, ShowIf( "HasMultipleBullets" )] private float bulletSeparation = 0.5f;
	[SerializeField, ShowIf( "HasMultipleBullets" )] private float fov = 45f;

	private bool HasMultipleBullets { get { return attackNumber > 1; } }

	private Camera camera;
	private Collider collider;
	private Rigidbody rigidbody;

	protected void ShootAttack()
	{
		if ( !CheckCanAttack() ) return;

		normalizedCharge = Mathf.Clamp( charge / chargeTime, 0.0f, chargeTime );
		Quaternion rotation = pivot.rotation;

		if ( aimScreenCenter )
		{
			Ray ray = camera.ViewportPointToRay( new Vector3( 0.5f, 0.5f ) );
			if ( Physics.Raycast( ray, out RaycastHit hit, Mathf.Infinity ) )
			{
				rotation = Quaternion.LookRotation( (hit.point - pivot.position).normalized, Vector3.up );
			}
		}

		Vector3 localRight = pivot.transform.right;

		if ( attackNumber % 2 == 1 ) // ODD
		{
			GameObject newShoot = Instantiate( projectilePrefab, pivot.position + pivot.forward, rotation );
			FilterProjectile( newShoot );

			int shootsPerSide = (attackNumber - 1) / 2;

			for ( int i = -1; i >= -shootsPerSide; i-- )
			{
				Vector3 newShotPos = pivot.position + localRight * bulletSeparation * i + pivot.forward;
				newShoot = Instantiate( projectilePrefab, newShotPos, Quaternion.LookRotation( (newShotPos - pivot.position).normalized, pivot.up ) );
				FilterProjectile( newShoot );
			}

			for ( int i = 1; i <= shootsPerSide; i++ )
			{
				Vector3 newShotPos = pivot.position + localRight * bulletSeparation * i + pivot.forward;
				newShoot = Instantiate( projectilePrefab, newShotPos, Quaternion.LookRotation( (newShotPos - pivot.position).normalized, pivot.up ) );
				FilterProjectile( newShoot );
			}
		}
		else
		{ // EVEN
			int shootsPerSide = attackNumber / 2;

			for ( int i = -shootsPerSide; i <= shootsPerSide; i++ )
			{
				if ( i == 0 ) continue;
				GameObject newShoot = Instantiate( projectilePrefab, pivot.position + localRight * bulletSeparation * i, rotation );
				FilterProjectile( newShoot );
			}
		}

		ResetCharge();
		ActivateCooldown();
	}

	public override void Start(GameObject controllerObject, Transform pivot)
	{
		base.Start( controllerObject, pivot );

		camera = Camera.main;
		collider = controllerObject.GetComponent<Collider>();

		if ( carryVelocity )
			rigidbody = controllerObject.GetComponent<Rigidbody>();

		if ( projectilePrefab == null ) Debug.LogError( $"WEAPON GUN :: Missing attack prefab." );
		if ( camera == null ) Debug.LogError( $"WEAPON GUN :: {name} :: Missing camera." );
		if ( collider == null ) Debug.LogError( $"WEAPON GUN :: {name} :: Missing collider." );
		if ( chargeOnHold && chargeable == null ) Debug.LogError( $"WEAPON GUN :: {name} :: Missing chargeable." );
		if ( carryVelocity && rigidbody == null ) Debug.LogError( $"WEAPON GUN :: {name} :: Missing rigidbody." );
	}

	private void FilterProjectile(GameObject go)
	{
		go.name = "Shoot";
		Physics.IgnoreCollision( go.GetComponent<Collider>(), collider );

		if ( chargeOnHold )
			chargeable.Apply( normalizedCharge, go );

		if ( carryVelocity )
		{
			float rbVelMagnitude = rigidbody.velocity.magnitude;
			if ( rbVelMagnitude <= minCarryVelocity ) return;

			Vector3 velocityXZ = rigidbody.velocity;
			Vector3 forwardXZ = pivot.forward;

			velocityXZ.y = forwardXZ.y = 0.0f;

			float scale = 1f +
				Vector3.Dot( forwardXZ.normalized, velocityXZ.normalized )
				* carryVelocityCurve.Evaluate( Mathf.InverseLerp( 0.0f, maxCarrierVelocity, rbVelMagnitude ) )
				* carryVelocityRatio;
			go.GetComponent<SpawnImpulse>().Scale( Vector3.one * scale );
		}
	}

	private void ResetCharge()
	{
		charge = 0.0f;
		normalizedCharge = 0.0f;
	}

	public override void OnAttackDown()
	{
		if ( attackOnDown )
			ShootAttack();
	}

	public override void OnAttackHold()
	{
		if ( attackOnHold )
			ShootAttack();
		else if ( chargeOnHold )
			charge += Time.deltaTime;
	}

	public override void OnAttackUp()
	{
		if ( attackOnUp )
			ShootAttack();
	}
}
