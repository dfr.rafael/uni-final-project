using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent( typeof( WeaponController ) )]
public class WeaponActivateTimer : MonoBehaviour
{

	[SerializeField]
	private float attacksPerSecond = 0;

	[SerializeField]
	private float specialsPerSecond = 0;

	private WeaponController controller;
	private float attackTimer = 0f;
	private float specialTimer = 0f;

	private Coroutine attackCoroutine;
	private Coroutine specialCoroutine;

	void Awake()
	{
		controller = GetComponent<WeaponController>();
	}

	private void OnEnable()
	{
		if ( attacksPerSecond > 0f )
			attackCoroutine = StartCoroutine( AttackTimer() );

		if ( specialsPerSecond > 0f )
			specialCoroutine = StartCoroutine( SpecialTimer() );
	}

	private void OnDisable()
	{
		if ( attackCoroutine != null )
			StopCoroutine( attackCoroutine );

		if ( specialCoroutine != null )
			StopCoroutine( specialCoroutine );
	}

	private IEnumerator AttackTimer()
	{
		bool stop = false;
		float t = 1.0f / attacksPerSecond;
		while ( !stop )
		{
			attackTimer += Time.deltaTime;
			if ( attackTimer > t )
			{
				controller.SingleAttack();
				attackTimer = 0f;
			}
			yield return new WaitForEndOfFrame();
		}
	}

	private IEnumerator SpecialTimer()
	{
		bool stop = false;
		float t = 1.0f / specialsPerSecond;
		while ( !stop )
		{
			specialTimer += Time.deltaTime;
			if ( specialTimer > t )
			{
				controller.SingleSpecial();
				specialTimer = 0f;
			}
			yield return new WaitForEndOfFrame();
		}
	}
}
