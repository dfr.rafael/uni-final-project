using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;

public class WeaponController : MonoBehaviour
{

	[SerializeField, NaughtyAttributes.Required]
	private Transform weaponPivot;
	[SerializeField, NaughtyAttributes.Required]
	private IWeapon currentAttack;
	[SerializeField]
	private IWeapon currentSpecial;

	[SerializeField, Tooltip( "Energy increase gain per second" )]
	private int energyGain = 20;

	[SerializeField]
	private int maxEnergy = 100;
	public int energy { get; private set; } = 0;

	[SerializeField, Tooltip( "Energy gain cooldown after attacking." )]
	private float energyStopCooldown = 0.2f;
	private float energyGainTimer = 0.0f;


	[SerializeField]
	private UnityEvent<int> onEnergyChangedAbsolute;

	[SerializeField]
	private UnityEvent<float> onEnergyChangedNormalized;

	public bool canUseAttack { get; private set; } = true;
	public bool canUseSpecial { get; private set; } = true;

	public bool isAttackHeld { get; private set; } = false;
	public bool isSpecialHeld { get; private set; } = false;

	private void Start()
	{
		if ( currentAttack == null )
		{
			Debug.LogError( $"WEAPON CONTROLLER :: {name} :: Missing attack!" );
		}
		else
		{
			currentAttack = Instantiate( currentAttack );
			currentAttack.Start( gameObject, weaponPivot );
		}

		if ( currentSpecial != null )
		{
			currentSpecial = Instantiate( currentSpecial );
			currentSpecial.Start( gameObject, weaponPivot );
		}

		energy = maxEnergy;
	}

	private void Update()
	{
		if ( energyGainTimer > 0 )
		{
			energyGainTimer -= Time.deltaTime;
		}
		else
		{
			energy += Mathf.CeilToInt( energyGain * Time.deltaTime );
			energy = Mathf.Clamp( energy, 0, maxEnergy );
			InvokeCallbacks();
		}

		currentAttack.Update();

		if ( currentSpecial != null )
			currentSpecial.Update();
	}

	public void ChangeAttack(IWeapon weapon)
	{
		currentAttack.Destroy();
		StartCoroutine( NextFrameCallback( () =>
		{
			currentAttack = Instantiate( weapon );
			currentAttack.Start( gameObject, weaponPivot );
		} ) );

	}

	public void ChangeSpecial(IWeapon weapon)
	{
		currentSpecial.Destroy();
		StartCoroutine( NextFrameCallback( () =>
		{
			currentSpecial = Instantiate( weapon );
			currentSpecial.Start( gameObject, weaponPivot );
		} ) );
	}

	private IEnumerator NextFrameCallback(System.Action callback)
	{
		yield return new WaitForEndOfFrame();
		callback.Invoke();
	}

	public void SingleAttack()
	{
		currentAttack.OnAttackDown();
		currentAttack.OnAttackUp();
	}

	public void SingleSpecial()
	{
		currentSpecial.OnAttackDown();
		currentSpecial.OnAttackUp();
	}

	public void OnAttack(InputAction.CallbackContext context)
	{
		if ( context.phase == InputActionPhase.Performed )
		{
			isAttackHeld = true;
			currentAttack.OnAttackDown();
		}
		else if ( context.phase == InputActionPhase.Canceled )
		{
			isAttackHeld = false;
			currentAttack.OnAttackUp();
		}
	}

	public void OnSpecial(InputAction.CallbackContext context)
	{
		if ( context.phase == InputActionPhase.Performed )
		{
			isSpecialHeld = true;
			currentSpecial.OnAttackDown();
		}
		else if ( context.phase == InputActionPhase.Canceled )
		{
			isSpecialHeld = false;
			currentSpecial.OnAttackUp();
		}
	}

	public void ActivateAttackCooldown()
	{
		StopCoroutine( AttackCooldown() );
		StartCoroutine( AttackCooldown() );
	}

	public void ActivateSpecialCooldown()
	{
		StopCoroutine( SpecialCooldown() );
		StartCoroutine( SpecialCooldown() );
	}

	// Checks wheter the attack can be used. Subtracts the attack cost from energy if it can.
	public bool CheckAttackCost()
	{
		if ( energy > currentAttack.energyPerAction )
		{
			energy -= currentAttack.energyPerAction;
			InvokeCallbacks();
			return true;
		}
		return false;
	}

	public bool CheckSpecialCost()
	{
		if ( energy > currentSpecial.energyPerAction )
		{
			energy -= currentSpecial.energyPerAction;
			InvokeCallbacks();
			return true;
		}
		return false;
	}

	private void InvokeCallbacks()
	{
		onEnergyChangedAbsolute.Invoke( energy );
		onEnergyChangedNormalized.Invoke( energy / (float)maxEnergy );
	}

	private IEnumerator AttackCooldown()
	{
		StopEnergyGain();
		canUseAttack = false;

		float cooldownSeconds = 1 / currentAttack.actionsPerSecond;
		float counter = 0;
		while ( counter < cooldownSeconds )
		{
			yield return new WaitForEndOfFrame();
			counter += Time.deltaTime;
		}

		canUseAttack = true;
	}

	private IEnumerator SpecialCooldown()
	{
		StopEnergyGain();
		canUseSpecial = false;

		float cooldownSeconds = 1 / currentAttack.actionsPerSecond;
		float counter = 0;
		while ( counter < cooldownSeconds )
		{
			yield return new WaitForEndOfFrame();
			counter += Time.deltaTime;
		}

		canUseSpecial = true;
	}

	public void StopEnergyGain()
	{
		energyGainTimer = energyStopCooldown;
	}
}
