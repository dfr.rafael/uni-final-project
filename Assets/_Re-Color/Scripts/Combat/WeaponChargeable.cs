using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Weapons/Effect/Chargeable", fileName = "Chargeable Effect")]
public class WeaponChargeable : ScriptableObject
{

	[SerializeField]
	private bool affectSwawnImpulse = true;

	[SerializeField, NaughtyAttributes.ShowIf( "affectSwawnImpulse" )]
	private Vector3 spawnImpulseMult = Vector3.one;

	// Charge ranging [0,1]
	public void Apply(float charge, GameObject prefab)
	{
		if ( affectSwawnImpulse )
			prefab.GetComponent<SpawnImpulse>()?.Scale( Vector3.Lerp(Vector3.one, spawnImpulseMult, charge) );
	}
}
