using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponChanger : MonoBehaviour
{
	[SerializeField]
	private IWeapon weapon;

	public void Change(GameObject weaponHolder)
	{
		if ( weapon.IsSpecial )
			weaponHolder.GetComponent<WeaponController>()?.ChangeSpecial( weapon );
		else
			weaponHolder.GetComponent<WeaponController>()?.ChangeAttack( weapon );
	}
}
