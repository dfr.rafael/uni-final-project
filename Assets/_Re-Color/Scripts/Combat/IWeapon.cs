using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IWeapon : ScriptableObject
{

	protected WeaponController controller;

	[Header( "Weapon General Config" )]
	[SerializeField] private bool isSpecial = false;
	[SerializeField] protected bool attackOnDown = true;
	[SerializeField] protected bool attackOnHold = true;
	[SerializeField] protected bool attackOnUp = false;

	[Header( "Weapon Energy" )]
	[SerializeField] public float actionsPerSecond = 1.0f;
	[SerializeField] public int energyPerAction = 10;

	protected Transform pivot;

	public bool IsSpecial { get { return isSpecial; } }

	public virtual void Start(GameObject controllerObject, Transform pivot)
	{
		controller = controllerObject.GetComponent<WeaponController>();

		if ( controller == null )
		{
			Debug.LogError( $"WEAPON :: {name} :: Weapon controller not found." );
		}

		this.pivot = pivot;
	}

	public virtual void Update()
	{
		bool isHeld = isSpecial ? controller.isSpecialHeld : controller.isAttackHeld;
		if ( isHeld ) { OnAttackHold(); }
	}

	public virtual void Destroy() { }

	public virtual void OnAttackDown() { }
	public virtual void OnAttackUp() { }
	public virtual void OnAttackHold() { }

	protected void ActivateCooldown()
	{
		if ( isSpecial )
			controller.ActivateSpecialCooldown();
		else
			controller.ActivateAttackCooldown();
	}

	protected bool CheckCanAttack()
	{
		if ( isSpecial )
		{
			if ( !controller.canUseSpecial ) return false;
			if ( !controller.CheckSpecialCost() ) return false;
		}
		else
		{
			if ( !controller.canUseAttack ) return false;
			if ( !controller.CheckAttackCost() ) return false;
		}
		return true;
	}

}
