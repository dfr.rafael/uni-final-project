﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ColorExtensionMethods {
	public static Color Clamp01(this Color color)
	{
		color.r = Mathf.Clamp01(color.r);
		color.g = Mathf.Clamp01(color.g);
		color.b = Mathf.Clamp01(color.b);
		color.a = Mathf.Clamp01(color.a);
		return color;
	}
}
