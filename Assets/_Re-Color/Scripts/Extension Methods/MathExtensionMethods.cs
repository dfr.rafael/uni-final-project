﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathExtensionMethods {
	public static float Sign(this float f)
	{
		if (f == 0) return 0;
		if (f < 0) return -1;
		return 1;
	}
}
