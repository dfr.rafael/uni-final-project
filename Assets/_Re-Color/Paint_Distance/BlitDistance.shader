﻿Shader "Paint/BlitDistance"
{
	Properties
	{
		_MainTex( "Texture", 2D ) = "white" {}
		[Range( 0,1 )]
		_Strength( "Strenth", float ) = 1
		_Scale( "Scale", float ) = 1
	}

		CGINCLUDE

#pragma multi_compile CNOISE PNOISE SNOISE BCCNOISE4 BCCNOISE8
#pragma multi_compile _ THREED
#pragma multi_compile _ FRACTAL
#pragma multi_compile _ GRAD_NUMERICAL GRAD_ANALYTICAL

#include "UnityCG.cginc"

#if defined(CNOISE) || defined(PNOISE)

#if defined(THREED)
#include "Packages/jp.keijiro.noiseshader/Shader/ClassicNoise3D.hlsl"
#else
#include "Packages/jp.keijiro.noiseshader/Shader/ClassicNoise2D.hlsl"
#endif

#define INITIAL_WEIGHT 0.5

#if defined(GRAD_ANALYTICAL)
#define NOISE_FUNC(coord, period) 0
#elif defined(CNOISE)
#define NOISE_FUNC(coord, period) cnoise(coord)
#else // PNOISE
#define NOISE_FUNC(coord, period) pnoise(coord, period)
#endif

#endif

#if defined(SNOISE)

#if defined(THREED)
#include "Packages/jp.keijiro.noiseshader/Shader/SimplexNoise3D.hlsl"
#else
#include "Packages/jp.keijiro.noiseshader/Shader/SimplexNoise2D.hlsl"
#endif

#define INITIAL_WEIGHT 0.25

#if defined(GRAD_ANALYTICAL)
#define NOISE_FUNC(coord, period) snoise_grad(coord)
#else
#define NOISE_FUNC(coord, period) snoise(coord)
#endif

#endif

#if defined(BCCNOISE4)

#include "Packages/jp.keijiro.noiseshader/Shader/BCCNoise4.hlsl"

#define INITIAL_WEIGHT 0.25

#if defined(THREED)
#if defined(GRAD_ANALYTICAL)
#define NOISE_FUNC(coord, period) (Bcc4NoiseClassic(coord).xyz)
#else
#define NOISE_FUNC(coord, period) (Bcc4NoiseClassic(coord).w)
#endif
#else
#if defined(GRAD_ANALYTICAL)
#define NOISE_FUNC(coord, period) (Bcc4NoisePlaneFirst(float3(coord, 0)).xy)
#else
#define NOISE_FUNC(coord, period) (Bcc4NoisePlaneFirst(float3(coord, 0)).w)
#endif
#endif

#endif

#if defined(BCCNOISE8)

#include "Packages/jp.keijiro.noiseshader/Shader/BCCNoise8.hlsl"

#define INITIAL_WEIGHT 0.25

#if defined(THREED)
#if defined(GRAD_ANALYTICAL)
#define NOISE_FUNC(coord, period) (Bcc8NoiseClassic(coord).xyz)
#else
#define NOISE_FUNC(coord, period) (Bcc8NoiseClassic(coord).w)
#endif
#else
#if defined(GRAD_ANALYTICAL)
#define NOISE_FUNC(coord, period) (Bcc8NoisePlaneFirst(float3(coord, 0)).xy)
#else
#define NOISE_FUNC(coord, period) (Bcc8NoisePlaneFirst(float3(coord, 0)).w)
#endif
#endif

#endif


		sampler2D _MainTex;
		float4 _MainTex_ST;
		float4 _MainTex_TexelSize;

		fixed4 _MainColor;
		float _Strength;
		sampler2D _PaintTexture;
		float4 _PaintTexture_ST;

		float4 _Center;
		float _Hardness;
		float _Radius;
		float _Scale = 1.0;
		float4x4 _ModelMatrix;
		int _BlendOp = 4;

		struct VertexData {
			float4 uv : TEXCOORD0;
			float4 vertex : POSITION;
		};

		struct Interpolators {
			float4 vertex : SV_POSITION;
			float2 uv : TEXCOORD0;
			float4 wPos : TEXCOORD1;
		};

		float SphereMask( float3 position, float3 center, float radius, float hardness ) {
			return 1 - saturate( ( distance( position, center ) - radius ) / ( 1 - hardness ) );
		}

		float Attenuation( float3 position, float3 center, float radius, float linearFalloff, float quadraticFalloff ) {
			float d = 1.0 - saturate( distance( position, center ) / radius );
			return d;
		}

		float4 smoothSample( float2 uv ) {
			return smoothstep( 0.5 - 0.01, 0.5 + 0.01, tex2D( _MainTex, uv ) );
		}


		float4 fragDownsample( Interpolators i ) : COLOR
		{

			float2 size = _MainTex_TexelSize.xy;

			float4 sdf = smoothSample( i.uv + size * float2( -3, -3 ) );
			sdf += smoothSample( i.uv + size * float2( -3, -1 ) );
			sdf += smoothSample( i.uv + size * float2( -3, 1 ) );
			sdf += smoothSample( i.uv + size * float2( -3, 3 ) );

			sdf += smoothSample( i.uv + size * float2( -1, -3 ) );
			sdf += smoothSample( i.uv + size * float2( -1, -1 ) );
			sdf += smoothSample( i.uv + size * float2( -1, 1 ) );
			sdf += smoothSample( i.uv + size * float2( -1, 3 ) );

			sdf += smoothSample( i.uv + size * float2( 1, -3 ) );
			sdf += smoothSample( i.uv + size * float2( 1, -1 ) );
			sdf += smoothSample( i.uv + size * float2( 1, 1 ) );
			sdf += smoothSample( i.uv + size * float2( 1, 3 ) );

			sdf += smoothSample( i.uv + size * float2( 3, -3 ) );
			sdf += smoothSample( i.uv + size * float2( 3, -1 ) );
			sdf += smoothSample( i.uv + size * float2( 3, 1 ) );
			sdf += smoothSample( i.uv + size * float2( 3, 3 ) );

			// keep it a nice high value so the mips are readable
			sdf *= 0.0625; // 1.0f / 16.0f -> since 16 values are read

			return sdf;
		}

		float4 fragCompile( Interpolators i ) : COLOR
		{

			float2 size = _MainTex_TexelSize.xy;

			float4 sdf = tex2D( _MainTex, i.uv );

			float total = 0.00001;
			total += sdf.x + sdf.y + sdf.z + sdf.w;
			sdf *= 1.0 / total;

			return sdf;
		}

		Interpolators VertexProgram( VertexData v ) {
			Interpolators o;

			o.wPos = mul( unity_ObjectToWorld, v.vertex );
			o.uv = v.uv;
			float4 uv = float4( 0, 0, 0, 1 );
			uv.xy = float2( 1, _ProjectionParams.x ) * ( v.uv.xy * float2( 2, 2 ) - float2( 1, 1 ) );
			o.vertex = uv;
			return o;
		}

		Interpolators VertexProgramImage( VertexData v )
		{
			Interpolators o;
			o.vertex = UnityObjectToClipPos( v.vertex );
			o.uv = v.uv.xy;
			return o;
		}

		ENDCG

			SubShader
		{
			Cull Off ZWrite Off ZTest Off
			
			Pass // 0 - Paint
			{
				Name "Splat"

				Conservative True
				BlendOp[_BlendOp]
				Blend One Zero

				CGPROGRAM
				#pragma vertex VertexProgram
				#pragma fragment FragmentProgram
				#pragma target 3.0

				fixed4 FragmentProgram( Interpolators i ) : SV_Target
				{


				const float epsilon = 0.0001;

				float timeScale = 1000.0;
			#if defined(THREED)
				float3 uv = i.wPos.xyz * 2.0 + float3( timeScale, timeScale * 2.0, timeScale * 3.0 ) * _Time.y;
			#else
				float2 uv = i.wPos.xy * 2.0 + float2( timeScale, timeScale ) * _Time.y;
			#endif

			#if defined(GRAD_ANALYTICAL) || defined(GRAD_NUMERICAL)
				#if defined(THREED)
					float3 o = 0.5;
				#else
					float2 o = 0.5;
				#endif
			#else
				float o = 0.5;
			#endif

			float s = _Scale;
			float w = INITIAL_WEIGHT;

			#ifdef FRACTAL
			for ( int i = 0; i < 6; i++ )
			#endif
			{
				#if defined(THREED)
					float3 coord = float3( uv * s );
					float3 period = float3( s, s, 1.0 ) * 2.0;
				#else
					float2 coord = uv * s;
					float2 period = s * 2.0;
				#endif

				#if defined(GRAD_NUMERICAL)
					#if defined(THREED)
						float v0 = NOISE_FUNC( coord, period );
						float vx = NOISE_FUNC( coord + float3( epsilon, 0, 0 ), period );
						float vy = NOISE_FUNC( coord + float3( 0, epsilon, 0 ), period );
						float vz = NOISE_FUNC( coord + float3( 0, 0, epsilon ), period );
						o += w * float3( vx - v0, vy - v0, vz - v0 ) / epsilon;
					#else
						float v0 = NOISE_FUNC( coord, period );
						float vx = NOISE_FUNC( coord + float2( epsilon, 0 ), period );
						float vy = NOISE_FUNC( coord + float2( 0, epsilon ), period );
						o += w * float2( vx - v0, vy - v0 ) / epsilon;
					#endif
				#else
					o += NOISE_FUNC( coord, period ) * w;
				#endif

				s *= 2.0;
				w *= 0.5;
			}

			float4 noiseValue = float4( 0,0,0,0 );
			#if defined(GRAD_ANALYTICAL) || defined(GRAD_NUMERICAL)
				#if defined(THREED)
					noiseValue = float4( o, 1 );
				#else
					noiseValue = float4( o, 1, 1 );
				#endif
			#else
				noiseValue = float4( o, o, o, 1 );
			#endif

					float att = Attenuation( i.wPos, _Center.xyz,  _Radius, 10.0, 5.0 ) * 5;
					float4 colorTo = float4( 1, 1, 1, 1 );
					float4 color = lerp( _MainColor, colorTo, att );

					float strength = _MainColor.a;

					if ( strength < 0 )
					{
						return saturate( float4( 1, 1, 1, 1 ) - ( att * color * noiseValue * abs( strength ) ) );
					}
					return saturate( att * color * noiseValue * strength );
				}
				ENDCG
			}

			Pass // 1 - Paint UVs as Black
			{
				Name "PaintBlack"

				Conservative True
				BlendOp Min
				Blend One Zero

				CGPROGRAM
				#pragma vertex VertexProgram
				#pragma fragment FragmentBlackProgram
				#pragma target 3.0

				fixed4 FragmentBlackProgram( Interpolators i ) : SV_Target
				{
					return fixed4( 0.0, 0.0, 0.0, 1.0 );
				}
				ENDCG
			}

			Pass //Pass 2 - downsample pass
			{
				Name "Downsample"
				CGPROGRAM
				#pragma fragmentoption ARB_precision_hint_fastest
				#pragma vertex VertexProgramImage
				#pragma fragment fragDownsample
				#pragma target 3.0
				ENDCG
			}

			Pass // 3 - compile pass
			{
				Name "Compile"
				CGPROGRAM
				#pragma fragmentoption ARB_precision_hint_fastest
				#pragma vertex VertexProgramImage
				#pragma fragment fragCompile
				#pragma target 3.0
				ENDCG
			}

		}
}
