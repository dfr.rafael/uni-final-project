using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

[RequireComponent( typeof( PaintDistanceReceiver ) )]
public class PaintOverTime : MonoBehaviour
{

	[SerializeField] AnimationCurve curve = AnimationCurve.Linear( 0.0f, 0.0f, 1.0f, 1.0f );
	[SerializeField] float seconds = 1f;

	private PaintDistanceReceiver paintReceiver;

	void Start()
	{
		paintReceiver = GetComponent<PaintDistanceReceiver>();
	}

	[NaughtyAttributes.Button()]
	public void Execute()
	{
		StartCoroutine( StartPainting() );
	}

	private IEnumerator StartPainting()
	{
		float counter = 0;
		float t = Time.fixedDeltaTime / seconds;
		while ( counter < seconds )
		{
			float strength = (1.0f - curve.Evaluate( counter )) * -1f * t;
			//float strength = t * -1f;

			Shader.SetGlobalVector( "_Center", (Vector4)transform.position );
			Shader.SetGlobalFloat( "_Hardness", 1.0f );
			Shader.SetGlobalFloat( "_Radius", Mathf.Infinity );
			Color color = Color.black;
			color.a = strength;
			Shader.SetGlobalColor( "_MainColor", color );
			Shader.SetGlobalInt( "_BlendOp", strength >= 0.0f ? (int)BlendOp.Max : (int)BlendOp.Min );

			paintReceiver.Splat();

			yield return new WaitForFixedUpdate();
			counter += Time.fixedDeltaTime;
		}
	}
}
