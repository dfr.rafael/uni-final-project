﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Events;

public class PaintDistanceReceiver : MonoBehaviour
{

	private RenderTexture currentTexture;

	[SerializeField]
	private int width = 1024, height = 1024;

	[NonSerialized]
	private Material blitMaterial;

	private CommandBuffer command;

	public float sizeScale = 1.0f;

	private bool isPaintable = true;

	//
	// CALLBACKS
	//
	[SerializeField] private UnityEvent onPercentageChange;
	[SerializeField] private UnityEvent<float> onPercentageChangeParameter;
	[SerializeField] private UnityEvent onPercentageMax;

	//
	// PERCENTAGE
	//
	private Coroutine computeCoroutine;
	private RenderTexture scoreTex;
	private Texture2D Tex4;

	[SerializeField, Tooltip( "Value over which the object will be considered fully painted." )]
	private float thresholdPaintPercentage = .95f;
	private float startPaintPercentage = 0.0f;
	private float currentPaintPercentage = .0f;

	public float PaintPercentage
	{
		get
		{
			return currentPaintPercentage;
		}
	}

	private const float computePaintTimer = .05f;

	// 
	// NOISE
	//
	public enum NoiseType
	{ ClassicPerlin, PeriodicPerlin, Simplex, FastSimplex, SuperSimplex }

	public enum GradientType { None, Numerical, Analytical }

	[SerializeField] NoiseType _noiseType = NoiseType.ClassicPerlin;
	[SerializeField] GradientType _gradientType = GradientType.None;
	[SerializeField] bool _is3D = false;
	[SerializeField] bool _isFractal = false;
	[SerializeField] Shader shader = null;

	private void Awake()
	{
		currentTexture = new RenderTexture( width, height, 0, RenderTextureFormat.ARGBFloat );
		currentTexture.Create();
		GetComponent<Renderer>().material.SetTexture( "_MainTexture", currentTexture );

		blitMaterial = new Material( Shader.Find( "Paint/BlitDistance" ) );
		blitMaterial.hideFlags = HideFlags.HideAndDontSave;
		blitMaterial.shaderKeywords = null;

		SetupPercentageTextures();
		StartCoroutine( GetPaintPercentageNextFrame() );
	}

	private void Start()
	{
		SetCommandBuffer();
		SetupTexture();
	}

	private void SetupPercentageTextures()
	{
		scoreTex = new RenderTexture( width / 8, height / 8, 0, RenderTextureFormat.ARGBFloat, RenderTextureReadWrite.Linear );
		scoreTex.autoGenerateMips = true;
		scoreTex.useMipMap = true;
		scoreTex.Create();

		Tex4 = new Texture2D( scoreTex.width, scoreTex.height, TextureFormat.RGBAFloat, false );
	}

	private void SetupTexture()
	{
		PaintTextureWhite();

		Renderer renderer = GetComponent<Renderer>();

		CommandBuffer commandBuffer = new CommandBuffer();
		commandBuffer.name = "Texture Paint UVs Black";
		commandBuffer.SetRenderTarget( currentTexture );
		commandBuffer.DrawRenderer( renderer, blitMaterial, 0, 1 );

		Graphics.ExecuteCommandBuffer( commandBuffer );
	}

	private IEnumerator GetPaintPercentageNextFrame()
	{
		yield return new WaitForEndOfFrame();
		startPaintPercentage = GetPaintPercentage();
	}

	public float GetCurrentPaintPercentage()
	{
		float percentage = GetPaintPercentage().Remap( startPaintPercentage, 1f, 0f, 1f );
		if ( percentage >= thresholdPaintPercentage )
		{
			SetPaintToMax();
			return 1.0f;
		}
		return percentage;
	}

	private void PaintTextureWhite()
	{
		RenderTexture.active = currentTexture;
		GL.Begin( GL.TRIANGLES );
		blitMaterial.SetPass( 0 ); // To avoid error in unity which SHOULD NOT exist
		GL.Clear( true, true, Color.white );
		GL.End();
		RenderTexture.active = null;
	}

	private void SetPaintToMax()
	{
		currentPaintPercentage = 1.0f;
		isPaintable = false;

		PaintTextureWhite();

		InvokeCallbacks();
		if ( onPercentageMax != null )
			onPercentageMax.Invoke();
	}

	/* GetPaintPercentageCPU
	public float GetPaintPercentageCPU()
	{
		RenderTexture.active = currentTexture;

		Texture2D texture = new Texture2D( width, height, TextureFormat.RGBAFloat, false );
		texture.ReadPixels( new Rect( 0, 0, width, height ), 0, 0 );

		Color totalColor = new Color( 0, 0, 0, 0 );
		for ( int x = 0; x < width; x++ )
		{
			for ( int y = 0; y < height; y++ )
			{
				totalColor += texture.GetPixel( x, y );
			}
		}

		float percentage = (totalColor.r + totalColor.g + totalColor.b) / (3.0f * width * height);
		return percentage;
	}
	*/

	public float GetPaintPercentage()
	{
		Graphics.Blit( currentTexture, scoreTex, blitMaterial, 2 ); // Downsample

		int texWidth = scoreTex.width;
		int texHeight = scoreTex.height;

		RenderTexture.active = scoreTex;
		Tex4.ReadPixels( new Rect( 0, 0, texWidth, texHeight ), 0, 0 );
		Tex4.Apply();
		RenderTexture.active = null;

		Color scoresColor = new Color( 0, 0, 0, 0 );

		for ( int x = 0; x < texWidth; x++ )
		{
			for ( int y = 0; y < texHeight; y++ )
			{
				scoresColor += Tex4.GetPixel( x, y );
			}
		}

		float percentage = (scoresColor.r + scoresColor.g + scoresColor.b) / (3.0f * texWidth * texHeight);
		return percentage;
	}

	private void SetCommandBuffer()
	{
		Renderer renderer = GetComponent<Renderer>();

		command = new CommandBuffer();
		command.name = "UV Space Renderer";
		command.SetRenderTarget( currentTexture );
		command.DrawRenderer( renderer, blitMaterial, 0, 0 );

		SetMaterialKeywords();
	}

	public void Splat()
	{
		if ( !isPaintable ) return;

		blitMaterial.SetFloat( "_Scale", sizeScale );
		Graphics.ExecuteCommandBuffer( command );

		if ( computeCoroutine != null )
			StopCoroutine( computeCoroutine );
		computeCoroutine = StartCoroutine( ComputePaintPercentage() );
	}

	private IEnumerator ComputePaintPercentage()
	{
		yield return new WaitForSeconds( computePaintTimer );
		currentPaintPercentage = GetCurrentPaintPercentage();

		InvokeCallbacks();
	}

	private void InvokeCallbacks()
	{
		if ( onPercentageChange != null )
			onPercentageChange.Invoke();

		if ( onPercentageChangeParameter != null )
			onPercentageChangeParameter.Invoke( currentPaintPercentage );
	}

	private void SetMaterialKeywords()
	{
		blitMaterial.shaderKeywords = null;

		if ( _noiseType == NoiseType.ClassicPerlin )
			blitMaterial.EnableKeyword( "CNOISE" );
		else if ( _noiseType == NoiseType.PeriodicPerlin )
			blitMaterial.EnableKeyword( "PNOISE" );
		else if ( _noiseType == NoiseType.Simplex )
			blitMaterial.EnableKeyword( "SNOISE" );
		else if ( _noiseType == NoiseType.FastSimplex )
			blitMaterial.EnableKeyword( "BCCNOISE4" );
		else if ( _noiseType == NoiseType.SuperSimplex )
			blitMaterial.EnableKeyword( "BCCNOISE8" );

		if ( _gradientType == GradientType.Analytical )
			blitMaterial.EnableKeyword( "GRAD_ANALYTICAL" );
		else if ( _gradientType == GradientType.Numerical )
			blitMaterial.EnableKeyword( "GRAD_NUMERICAL" );

		if ( _is3D )
			blitMaterial.EnableKeyword( "THREED" );

		if ( _isFractal )
			blitMaterial.EnableKeyword( "FRACTAL" );
	}

	private void OnDestroy()
	{
		currentTexture.Release();
	}
}
