using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using NaughtyAttributes;

public class Painter : MonoBehaviour
{

	[Range( 0.1f, 1f )]
	public float hardness;

	public float radius;

	public Color color;

	[Range( -1f, 1f )]
	public float strength = 0.5f;

	public bool onUpdate = false;

	private void Awake()
	{
		Shader.SetGlobalInt( "_BlendOp", (int)BlendOp.Max );
	}

	//void Update()
	//{
	//	if ( Input.GetKeyDown( KeyCode.A ) )
	//	{
	//		Shader.SetGlobalInt( "_BlendOp", (int)BlendOp.Add );
	//	}

	//	if ( Input.GetKeyDown( KeyCode.S ) )
	//	{
	//		Shader.SetGlobalInt( "_BlendOp", (int)BlendOp.ReverseSubtract );
	//	}

	//	if ( Input.GetKeyDown( KeyCode.M ) )
	//	{
	//		Shader.SetGlobalInt( "_BlendOp", (int)BlendOp.Max );
	//	}

	//	if ( onUpdate ) Activate();
	//}

	[Button( "Activate" )]
	public void Activate(bool setPosition = true)
	{

		Color[] colors = { Color.red, Color.green, Color.blue };
		color = colors[Random.Range( 0, colors.Length )];

		if ( setPosition )
		{
			Shader.SetGlobalVector( "_Center", (Vector4)transform.position );
		}
		Shader.SetGlobalInt( "_BlendOp", strength >= 0.0f ? (int)BlendOp.Max : (int)BlendOp.Min );
		Shader.SetGlobalFloat( "_Hardness", hardness );
		Shader.SetGlobalFloat( "_Radius", radius );
		color.a = strength;
		Shader.SetGlobalColor( "_MainColor", color );

		Collider[] colliders = Physics.OverlapSphere( transform.position, Mathf.Abs( radius ) );
		foreach ( Collider collider in colliders )
		{
			var receivers = collider.GetComponentsInChildren<PaintDistanceReceiver>();
			foreach ( var receiver in receivers )
				receiver.Splat();
		}
	}

	public void Activate(Vector3 position)
	{
		Shader.SetGlobalVector( "_Center", (Vector4)position );
		Activate( false );
	}


#if UNITY_EDITOR
	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere( transform.position, radius );
	}
#endif
}
