﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintDistanceCast : MonoBehaviour
{

	public float radius;
	[Range( 0f, 1f )]
	public float hardness;

	private new Camera camera;

	void Awake()
	{
		camera = Camera.main;
	}

	void Update()
	{
		if ( Input.GetMouseButton( 0 ) )
		{
			CastPaintSplash( radius );
		}
	}

	private Vector3 hitPoint;
	private void CastPaintSplash(float radius)
	{
		Ray cameraRay = camera.ScreenPointToRay( Input.mousePosition );
		if ( Physics.Raycast( cameraRay, out RaycastHit hitInfo ) )
		{
			hitPoint = hitInfo.point;
			//hitInfo.collider.GetComponent<PaintDistanceReceiver>()?.Blit( hitInfo.point, hardness, radius );
		}
	}

#if UNITY_EDITOR
	private void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		if ( hitPoint != null )
		{
			Gizmos.DrawWireSphere( hitPoint, radius );
		}
	}
#endif
}
